# pandas data frames
import masterthesis.dataframes.summary_dataframe as pd_summary
import masterthesis.dataframes.pandas_settings as pd_options
# experiments
import masterthesis.simulation as sim

# Enums and Descriptions
from masterthesis.pso.pso_enums import PsoEnums

# problems to optimize
import masterthesis.optimization_function.problem_collection as problems

# convenience stuff
import math
import time

from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription

FITNESS_EXPERIMENT_FLAG = False
ENERGY_EXPERIMENT_FLAG = False
ITERATION_EXPERIMENT_FLAG = True

# evaluation parameters
fitness_threshold = 1.5
max_pso_iterations = 25
start_energy = 300

# tuple keys
distance_key = 0
fitness_key = 1


def set_random_seed(seed=None):
    if seed is None:
        seed = int(round(time.time() * 1000))

    sim.random_seed_swarm = seed


def set_swarm_size(swarm_size):
    sim.swarm_size = swarm_size


# display the time the simulations ran in a readable format
def get_formatted_time_elapse_text(start_time, end_time):
    duration = end_time - start_time
    hours = int(duration // 3600)
    minutes = int((duration % 3600) // 60)
    seconds = round(duration % 60, 3)
    millisec = int((seconds - math.floor(seconds)) * 1000)
    seconds = math.floor(seconds)
    minutes = str(minutes) if minutes > 9 else "0" + str(minutes)
    seconds = str(seconds) if seconds > 9 else "0" + str(seconds)
    time_text = f"\nSimulation took {hours}h {minutes}m {seconds}s {millisec}ms to run"
    return time_text


# simulate a benchmark with and without scanning for a termination criterion
# Fitness threshold
# energy limit
# set amount of generations / iterations
def start_simulation(problem, swarm_type, uses_normalized_sampling, vehicle_type=None):
    if FITNESS_EXPERIMENT_FLAG:
        print("Fitness No Scan")
        sim.measure_while_driving = False
        sim.run(sim.simulate_with_fitness_threshold, problem, swarm_type, vehicle_type)
        print("Fitness w/ Scan")
        sim.measure_while_driving = True
        sim.normalized_sample_rate = uses_normalized_sampling
        sim.run(sim.simulate_with_fitness_threshold, problem, swarm_type, vehicle_type)

    if ENERGY_EXPERIMENT_FLAG:
        print("Energy No Scan")
        sim.measure_while_driving = False
        sim.run(sim.simulate_with_energy, problem, swarm_type, vehicle_type)
        print("Energy w/ Scan")
        sim.measure_while_driving = True
        sim.normalized_sample_rate = uses_normalized_sampling
        sim.run(sim.simulate_with_energy, problem, swarm_type, vehicle_type)

    if ITERATION_EXPERIMENT_FLAG:
        print("Iteration no scan")
        sim.measure_while_driving = False
        sim.run(sim.simulate_with_max_iterations, problem, swarm_type, vehicle_type)
        print("Iteration w/ scan")
        sim.measure_while_driving = True
        sim.normalized_sample_rate = uses_normalized_sampling
        sim.run(sim.simulate_with_max_iterations, problem, swarm_type, vehicle_type)


# simulate a specific model for a specific benchmark
def simulate_problem(problem, seed=None, swarm_size=10, swarm_type=PsoEnums.dubins, uses_normalized_sampling=False, vehicle_type=None, save_to_file=False):
    start_time = time.time()
    sim.panda_dataframe = None
    set_random_seed(seed)
    set_swarm_size(swarm_size)
    print(f"start with {problem}")

    start_simulation(problem, swarm_type, uses_normalized_sampling, vehicle_type)

    print("All Done")
    res = f"random seed for swarm: {sim.random_seed_swarm}\n"
    if save_to_file:
        path = f"./results/{sim.random_seed_swarm}_specific_{problem}.csv"
        pd_options.save_dataframe(pd_summary.sum_df, path)

    res += pd_summary.sum_df.__str__()
    end_time = time.time()
    res += get_formatted_time_elapse_text(start_time, end_time)
    print(res)
    txtfile = open(f"results/{swarm_type.name}_results_{swarm_size}_seed_{sim.random_seed_swarm}.txt", "w")
    txtfile.write(res)
    txtfile.close()


# Simulate all models for all benchmarks
def simulate_all(swarm_size=20, seed=None, uses_normalized_sampling=False, save_to_file=True, vehicle_type=None):
    start_time = time.time()
    sim.panda_dataframe = None
    all_problems = problems.get_problems()
    set_random_seed(seed)
    set_swarm_size(swarm_size)

    for swarm_type in PsoEnums:
        for problem_key in all_problems.keys():

            problem = all_problems[problem_key]

            print(f"start with {problem_key}")
            start_simulation(problem, swarm_type, uses_normalized_sampling, vehicle_type)

        print(swarm_type.name + " is done")

    print("All Done")
    res = f"random seed for swarm: {sim.random_seed_swarm}\n"
    if save_to_file:
        path = f"./results/{sim.random_seed_swarm}_all.csv"
        pd_options.save_dataframe(pd_summary.sum_df, path)

    res += pd_summary.sum_df.__str__()
    end_time = time.time()
    res += get_formatted_time_elapse_text(start_time, end_time)
    print(res)
    txtfile = open(f"results/all_results_{swarm_size}_seed_{sim.random_seed_swarm}.txt", "w")
    txtfile.write(res)
    txtfile.close()


global_simulate = False

if __name__ == "__main__":

    if global_simulate:
        simulate_all(swarm_size=20, seed=1609774586915, uses_normalized_sampling=False, save_to_file=True)

    else:
        # in case of changing parameters for a single benchmark
        prob = problems.get_problems()["Bukin N.6"]
        vt = VehicleDescription()
        vt.turn_radius = 1.0
        simulate_problem(prob, swarm_size=20, seed=1609774586915, uses_normalized_sampling=False, swarm_type=PsoEnums.dubins, save_to_file=True, vehicle_type=vt)

