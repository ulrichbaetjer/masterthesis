import math
from random import Random


def euclidean_distance(pos1, pos2):
    """
    calculates the euclidean distance between two points

    :param pos1: list with numbers
    :param pos2: list with numbers
    :return: euclidean distance between pos1 and pos2
    """
    squared_sum = 0
    for p1, p2 in zip(pos1, pos2):
        squared_sum += (p1 - p2) ** 2
    return math.sqrt(squared_sum)


def round_list(numbers, precision):
    """
    rounds all numbers of list to a given precision

    :param numbers: list of numbers
    :param precision: what to round to
    :return: copy of numbers with rounded numbers
    """
    res = []
    for n in numbers:
        res.append(round(n, precision))
    return res


def calculate_angle(start_point, end_point):
    """
    calculates the radian value between to points on a north faced map with the value 0 pointing towards east

    :param start_point: the start position, must be [x, y] list
    :param end_point: the goal position, must be [x, y] list
    :return: radian value from start to end or NaN if start and end are the same
    """
    if start_point == end_point:
        return math.nan

    right_half = start_point[0] <= end_point[0]
    upper_half = start_point[1] <= end_point[1]

    cath = abs(start_point[0] - end_point[0])
    ancath = abs(start_point[1] - end_point[1])

    angle = math.atan(ancath / cath) if cath > 0 else math.pi / 2

    # depending of the quadrant in the unit circle, we have to add an offset
    if right_half and not upper_half:
        angle = math.tau - angle

    elif not right_half and upper_half:
        angle = math.pi - angle

    elif not right_half and not upper_half:
        angle = math.pi + angle

    return angle


def get_angle(x, y):
    right_half = x >= 0
    upper_half = y >= 0

    y = abs(y)
    x = abs(x)
    angle = math.atan(y / x) if x > 0 else math.pi / 2
    print(x, y, angle)

    # depending of the quadrant in the unit circle, we have to add an offset
    if right_half and not upper_half:
        angle = math.tau - angle

    elif not right_half and upper_half:
        angle = math.pi - angle

    elif not right_half and not upper_half:
        angle = math.pi + angle

    return angle


def calculate_variance_and_average(numbers):
    """
    calculates the average and variance for a given list of numbers

    :param numbers: list containing numbers
    :return: variance and average value of list
    """
    avg = sum(numbers) / len(numbers)

    variance = 0
    for n in numbers:
        variance += (n - avg)**2
    variance /= len(numbers)

    return variance, avg


def create_random_seeds(seed, length):
    """
    Given a valid seed, creates a tuple of 31 floats between 0 and 1 which are used for the simulation seeds
    :param seed: a seed for the random number generator according to the python Random class
    :param length: the length
    :return: a list of random seeds with the given length
    """
    r = Random(seed)
    seeds = [r.random() for _ in range(length)]

    return seeds


if __name__ == "__main__":
    y = create_random_seeds(1234)
    print(y)
    for x in range(len(y)):
        print(y[x])
