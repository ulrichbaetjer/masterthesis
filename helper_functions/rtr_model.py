import masterthesis.helper_functions.list_functions as list_func
import math


class Rtr:
    '''
    The Rotation-Translate-Rotation model rotates at its start position until it faces its goal position
    Then it translates its position from start to goal
    At goal position it rotates until it faces its goal orientation
    '''
    def __init__(self, start, end, step):
        self.rotation_change = 0.0
        self.step_size = step
        self.path = []

        self.start = [s for s in start]
        self.end = [e for e in end]

        path = [tuple(self.start)]
        angle = list_func.calculate_angle(self.start[:-1], self.end[:-1])

        if math.isnan(angle):
            angle = self.start[-1]

        # rotate start towards goal
        x, y = self.start[0], self.start[1]
        for a in self.__rotate(self.start[-1], angle, self.step_size):
            path.append((x, y, a))

        # drive towards goal
        dist = list_func.euclidean_distance(self.start[:-1], self.end[:-1])
        steps = int(dist / self.step_size)
        if steps < 1:
            steps = 1
        step_x = (self.end[0] - self.start[0]) / steps
        step_y = (self.end[1] - self.start[1]) / steps
        for _ in range(steps):
            x += step_x
            y += step_y
            path.append((x, y, angle))

        # rotate end toward goal orientation
        x, y = self.end[0], self.end[1]
        for a in self.__rotate(angle, self.end[-1], self.step_size):
            path.append((x, y, a))

        self.path = path

        # total rotation change
        start_to_translate = abs((self.start[-1] % math.tau) - angle)
        translate_to_end = abs((self.end[-1] % math.tau) - angle)
        if start_to_translate > math.pi:
            start_to_translate = math.tau - start_to_translate
        if translate_to_end > math.pi:
            translate_to_end = math.tau - translate_to_end

        self.rotation_change = start_to_translate + translate_to_end

    def path_length(self):
        return list_func.euclidean_distance(self.start[:-1], self.end[:-1])

    def get_rotation_change(self):
        return self.rotation_change

    @staticmethod
    def __rotate(orient_start, orient_goal, o_steps=0.2):
        # make sure that everything is between 0 and tau
        orientation_start = orient_start % math.tau
        orientation_goal = orient_goal % math.tau

        rotation_distance = abs(orientation_start - orientation_goal)
        if rotation_distance > math.pi:
            rotation_distance = math.tau - rotation_distance

        if orientation_start > orientation_goal:
            o_steps = -o_steps

        rotation_points = []
        phi = orientation_start
        while rotation_distance > abs(o_steps):
            phi = (phi + o_steps) % math.tau
            rotation_points.append(phi)
            rotation_distance -= abs(o_steps)

        rotation_points.append(orientation_goal)
        return rotation_points
