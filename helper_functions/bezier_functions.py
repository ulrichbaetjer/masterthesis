import numpy as np
import masterthesis.helper_functions.list_functions as list_func


def calculate_control_points(start, end, turn):
    """
    calculates the control points for a bezier curve of degree 3
    :param start: start tuple consisting of x,y, theta
    :param end: tuple consisting of x,y, theta
    :param turn: turn radius of the curve
    :return: a fortran array with 4 control points
    """
    first_point = [start[0] + np.cos(start[2]) * turn, start[1] + np.sin(start[2]) * turn]

    second_point = [end[0] - np.cos(end[2]) * turn, end[1] - np.sin(end[2]) * turn]

    nodes = np.asfortranarray([
        [start[0], first_point[0], second_point[0], end[0]],
        [start[1], first_point[1], second_point[1], end[1]]
    ])

    return nodes


def calculate_samples(path, start, end, step_size):
    """
    Returns a list of 3-tuples of form (x, y, theta) along the bezier curve path
    :param path: bezier curve object
    :param start: tuple consisting of (x, y, theta)
    :param end:  tuple consisting of (x, y, theta)
    :param step_size: distance between two points along the bezier curve path
    :return: list of 3-tuples
    """
    path_sample = []
    s_vals = np.linspace(0.0, 1.0, num=int(path.length / step_size))
    points = path.evaluate_multi(s_vals)

    points = points.transpose()[1:]
    path_sample.append(start)
    for i, (x, y) in enumerate(points[:-1]):
        s = [x, y]
        e = points[i + 1].tolist()
        angle = list_func.calculate_angle(s, e)
        path_sample.append((x, y, angle))

    path_sample.append(end)
    return path_sample
