import math
import matplotlib as mpl


def get_transformation(ax, pos):
    degree = (pos[2] * 180) / math.pi

    x = pos[0]
    y = pos[1]

    # https://stackoverflow.com/questions/15557608/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-a
    # https://stackoverflow.com/questions/43000288/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-d
    ts = ax.transData

    coords = [x, y]
    tr = mpl.transforms.Affine2D().rotate_deg_around(coords[0], coords[1], degree)
    t = tr + ts

    return t