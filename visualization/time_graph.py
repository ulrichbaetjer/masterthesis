import matplotlib.pyplot as plt
import numpy as np

from masterthesis.pso.pso_enums import PsoEnums
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.optimization_function.problem_collection import *
import masterthesis.dataframes.pandas_settings as pd_options
import masterthesis.visualization.io_result_files as rw_files

line_color = 0
line_style = 1
line_marker = 2
line_mksize = 3

# Orientation:  (color, marker)
dict_lines = {
    str(OrientationEnums.greedy): ("#0000BB", "-", None, 1),
    str(OrientationEnums.no_update): ("#BB0000", "--", None, 1),
    str(OrientationEnums.default_pso): ("#00BB00", "-.", None, 1), # "#800080"
    str(OrientationEnums.random): ("#222222", ":", None, 1),
    str(OrientationEnums.towards_best_position): ("#FFA500", "-", "x", 3),
    str(OrientationEnums.straight_model_policy): ("#800080", "--", "+", 3), #"#00BB00"
    str(OrientationEnums.towards_swarm_center): ("#000000", "-.", "s", 3),
    rw_files.COMPARE_WITHOUT_SCAN: ("#888888", ":", ".", 3)
}

amount_xticks = 12
plt_xpad = 15   # amount of values padded to include the final value as xtick
measurements = ("Median", "Mean")
MEDIAN = 0
MEAN = 1


def create_title(model, problem, scan):
    title = model.upper() if model == "rtr" else model.title()
    title = title.replace("_", " ")

    title += " Model: " + problem + " "

    scan_correct = "w/ scan" if scan == "with_scan" else "w/o scan"

    title += scan_correct
    return title


def setup_plot(id, model, problem, scan, option):
    dfs = rw_files.get_dataframes(id, model, problem, scan)

    if scan == "with_scan":
        tmp_dfs = rw_files.get_dataframes(id, model, problem, "no_scan")
        if rw_files.is_fitness(option) or len(tmp_dfs) == 1:
            dfs.append(tmp_dfs[0])
        else:
            for tmp_df in tmp_dfs:
                if model == "rtr":
                    if tmp_df[pd_options.dict_orientation][0] == str(OrientationEnums.straight_model_policy):
                        dfs.append(tmp_df)
                else:
                    if tmp_df[pd_options.dict_orientation][0] == str(OrientationEnums.greedy):
                        dfs.append(tmp_df)
                        break

    plt.title(create_title(model, problem, scan))
    plt.gcf().set_size_inches(11, 8)

    fig = create_plot(dfs, scan, option)
    if is_saving:
        path = rw_files.create_path("time_distance", id, model, measures, scan, option)
        figname = rw_files.create_name(problem, scan)
        rw_files.save_image(fig, path, figname)
    plt.clf()


def create_plot(dataframes, scan, option):
    max_gen = dataframes[0][pd_options.dict_generation].max()
    gens = list(range(max_gen + 1))

    model = dataframes[0][pd_options.dict_model][0]
    max_val = 0
    has_shared_fitness = rw_files.is_fitness(option) and scan == "no_scan"
    for dataframe in dataframes:
        if has_shared_fitness and dataframe[pd_options.dict_orientation][0] != str(OrientationEnums.straight_model_policy):
            continue

        df = None
        if measures == measurements[MEDIAN]:
            df = dataframe.groupby(pd_options.dict_generation)[option].median()
        elif measures == measurements[MEAN]:
            df = dataframe.groupby(pd_options.dict_generation)[option].mean()

        if scan == "with_scan" and not dataframe[pd_options.dict_scan][0]:
            marker = dict_lines[rw_files.COMPARE_WITHOUT_SCAN]
            legend = rw_files.dict_long_names[rw_files.COMPARE_WITHOUT_SCAN]
        else:
            marker = dict_lines[dataframe[pd_options.dict_orientation][0]]
            legend = rw_files.dict_long_names[dataframe[pd_options.dict_orientation][0]]

        to_list = df.tolist()
        plt.plot(gens, to_list, color=marker[line_color], linestyle=marker[line_style], marker=marker[line_marker], markersize=marker[line_mksize], label=legend)
        max_df_val = max(to_list)
        if max_df_val > max_val:
            max_val = max_df_val
        if has_shared_fitness:
            break

    ax = plt.gca()
    ax.set_xlabel("Generation", fontsize=14)
    start, end = ax.get_xlim()
    ax.xaxis.set_ticks(np.arange(0, end-plt_xpad+1, (end-plt_xpad)//amount_xticks))

    ax.set_ylabel(rw_files.stylize_option(option, model), fontsize=14)

    if rw_files.is_fitness(option):
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')

    if not has_shared_fitness and len(dataframes) > 1:
        ax.legend()

    plt.xlim(0, max_gen)

    maxylim = min(max_val + 50, max_val * 1.1)

    plt.ylim(0, round(maxylim))
    fig = plt.gcf()
    fig.tight_layout()
    if not is_saving:
        plt.show()
    return fig


measures = measurements[MEDIAN]
is_saving = False

if __name__ == "__main__":
    pd_options.setup_dataframe()
    seed_id = 1609774586915
    measures = measurements[MEDIAN] # change to mean if wanted
    # opt = pd_options.dict_avg_dist
    # opt = pd_options.dict_avg_path_len
    # opt = pd_options.dict_longest_path_len
    # opt = pd_options.dict_fit
    for opt in [pd_options.dict_avg_dist, pd_options.dict_avg_path_len, pd_options.dict_longest_path_len, pd_options.dict_fit]:
        for model in PsoEnums:
            for problem in get_problems():
                for scan in ["with_scan", "no_scan"]:
                    setup_plot(seed_id, model.name, problem, scan, opt)
    print("done")
