# This file shows example paths for the dubins, reeds_shepp, bezier and rtr models

import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import matplotlib as mpl

import math
import dubins
import reeds_shepp
import bezier
import masterthesis.helper_functions.bezier_functions as bez_func
import masterthesis.helper_functions.rtr_model as rtr


################ EXAMPLE PATHS FOR MODELS #####################
sample_rate = 0.1
def get_dubins_plot(start, end, turn):
    # theta_1 = -math.pi/2
    # theta_2 = -math.pi/2
    # start = (-2, -0.5, theta_1)
    # end = (2, 0.5, theta_2)

    p = dubins.shortest_path(start, end, turn)
    samples, _ = p.sample_many(sample_rate)
    x_axis, y_axis = [[t[i] for t in samples] for i in (0, 1)]
    return x_axis, y_axis


def get_reeds_plot(start, end, turn):
    # theta_1 = -math.pi/2
    # theta_2 = math.pi/2
    # start = (-1, -1, theta_1)
    # end = (1, 1, theta_2)
    p = reeds_shepp.PyReedsSheppPath(start, end, turn)
    samples = p.sample(sample_rate)
    x_axis, y_axis = [[t[i] for t in samples] for i in (0, 1)]
    return x_axis, y_axis


def get_bezier_plot(start, end, turn):
    # theta_1 = -math.pi/2
    # theta_2 = math.pi/2
    # start = (-1, -1, theta_1)
    # end = (1, 1, theta_2)
    nodes = bez_func.calculate_control_points(start, end, turn)
    p = bezier.curve.Curve(nodes, degree=3)
    samples = bez_func.calculate_samples(p, start, end, sample_rate)
    x_axis, y_axis = [[t[i] for t in samples] for i in (0, 1)]
    return x_axis, y_axis


def get_rtr_plot(start, end, turn):
    p = rtr.Rtr(start, end, 0.1)
    samples = p.path
    x_axis, y_axis = [[t[i] for t in samples] for i in (0, 1)]
    return x_axis, y_axis
#################


scale = 20
alpha = 0.75


def calculate_polygon_size(ax):
    x_dist = abs(ax.get_xlim()[0] - ax.get_xlim()[1]) / scale
    y_dist = abs(ax.get_ylim()[0] - ax.get_ylim()[1]) / (scale * 5)
    return x_dist, y_dist


def get_polygon(ax, pos, col, scaling, alpha_level):

    x_dist, y_dist = calculate_polygon_size(ax)
    points = [[pos[0], pos[1]], [pos[0] - (x_dist / scaling), pos[1] + (y_dist / scaling)],
              [pos[0] - (x_dist / scaling), pos[1] - (y_dist / scaling)]]

    t = get_transformation(ax, pos)

    polygon = Polygon(points, fc=col, transform=t, edgecolor="black", alpha=alpha_level)
    return polygon


def get_transformation(ax, pos):
    degree = (pos[2] * 180) / math.pi

    x = pos[0]
    y = pos[1]

    # https://stackoverflow.com/questions/15557608/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-a
    # https://stackoverflow.com/questions/43000288/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-d
    ts = ax.transData
    # coords = ts.transform([x, y])
    # t = ts + tr
    coords = [x, y]
    tr = mpl.transforms.Affine2D().rotate_deg_around(coords[0], coords[1], degree)
    t = tr + ts

    return t


def draw_vehicle(fx):
    theta_1 = -math.pi/2
    theta_2 = math.pi/2
    pos = 0.25
    start = (-pos, -pos, theta_1)
    end = (pos, pos, theta_2)

    x_axis, y_axis = fx(start, end, 1)
    plt.plot(x_axis, y_axis, alpha=0.8)
    start_polygon = get_polygon(plt.gca(), start, "green", 1, 0.9)
    end_polygon = get_polygon(plt.gca(), end, "red", 1, 0.9)
    plt.gca().add_patch(start_polygon)
    plt.gca().add_patch(end_polygon)
    plt.gca().set_axis_off()
    plt.show()


if __name__ == "__main__":
    for f in [get_dubins_plot, get_reeds_plot, get_bezier_plot, get_rtr_plot]:
        draw_vehicle(f)
