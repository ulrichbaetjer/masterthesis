import pandas as pd
import os
import matplotlib.pyplot as plt

from masterthesis.pso.pso_enums import PsoEnums
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.optimization_function.problem_collection import *
import masterthesis.dataframes.pandas_settings as pd_options


dict_names = {
    "default_pso" : "PSO",
    "random": "RND",
    "towards_best_position": "GB",
    "towards_swarm_center": "TSC",
    "no_update": "NU",
    "greedy": "Greedy",
    "straight_model_policy": "SM"
}

COMPARE_WITHOUT_SCAN = "compare_without_scan"
dict_long_names = {
    str(OrientationEnums.default_pso): "PSO",
    str(OrientationEnums.random): "RND",
    str(OrientationEnums.towards_best_position): "GB",
    str(OrientationEnums.towards_swarm_center): "SC",
    str(OrientationEnums.no_update): "NU",
    str(OrientationEnums.greedy): "Greedy",
    str(OrientationEnums.straight_model_policy): "SM",
    COMPARE_WITHOUT_SCAN: "No Scan"
}


def stylize_option(opt, m):
    if opt == pd_options.dict_avg_path_len:
        return "Average Path Length" if m != PsoEnums.rtr.name else "Average Rotation"
    elif opt == pd_options.dict_avg_dist:
        return "Average Total Distance" if m != PsoEnums.rtr.name else "Average Total Rotation"
    elif opt == pd_options.dict_longest_path_len:
        return "Longest Path Length" if m != PsoEnums.rtr.name else "Longest Rotation"
    else:
        return opt


def stylize_title(model, problem, scan, orientation):
    s = "Without Scanning" if scan == "no_scan" else "With Scanning"
    o = orientation.name
    o = o.replace("_", " ")
    m = model.replace("_", " ")
    return f"{problem}: {m} Model {s} and {o}".title()


def is_fitness(option):
    return option in [pd_options.dict_fit, pd_options.dict_iqr75_fit, pd_options.dict_median_fit,
                      pd_options.dict_iqr25_fit, pd_options.dict_var_fit, pd_options.dict_avg_fit,
                      pd_options.dict_fit_min, pd_options.dict_fit_max]


def get_dataframes(id, model, problem, scan):
    path = f"../results/time_distance/{id}/{model}/{problem}/"
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]  # only use files not sub dirs
    files = [f for f in files if scan in f]  # split no_scan from with_scan

    names = [n[:n.index(scan) - 1] for n in files]
    names = [dict_names[n] for n in names]

    tuples = [(name, file) for name, file in zip(names, files)]
    tuples.sort()

    # names = [t[0] for t in tuples]
    files = [t[1] for t in tuples]

    dfs = [pd.read_csv(path + file) for file in files]
    return dfs


def get_short_names(dfs):
    names = []
    for df in dfs:
        s = df[pd_options.dict_orientation][0]
        s = s[s.index(".")+1:]
        names.append(dict_names[s])
    return names


def create_path(folder_name, id, model, measurement, scan, opt):
    file_path = os.path.join("../images", folder_name, f"{id}", f"{model}", f"{measurement}" f"{opt}", f"{scan}")
    return file_path


def create_name(problem, scan):
    p = problem.replace(" ", "_")
    p = p.replace(".", "")
    return f"{p}_{scan}"


def save_image(fig, file_path, name):
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    fig.savefig(file_path + f"/{name}")


if __name__ == "__main__":
    pd_options.setup_dataframe()
    seed_id = 1605471646019
    for model in PsoEnums:
        if model == PsoEnums.default or model == PsoEnums.straight:
            continue
        for problem in get_problems():
            for scan in ["no_scan", "with_scan"]:
                dfs = get_dataframes(seed_id, model.name, problem, scan)
                # get_names(dfs)
                break
            break
        break
