import matplotlib.pyplot as plt
import numpy as np
import seaborn as sbn
import masterthesis.optimization_function.problem_collection as problems
import matplotlib.ticker as ticker


def create_heatmap_array(problem, do_save_img=False):
    xmin = problem.get_min_values()[0]
    ymin = problem.get_min_values()[1]
    xmax = problem.get_max_values()[0]
    ymax = problem.get_max_values()[1]

    precision = 1002
    xrange = xmax - xmin
    yrange = ymax - ymin

    xstep = xrange / precision
    ystep = yrange / precision

    xlist = [xmin + xstep * i for i in range(precision)]
    ylist = [ymin + ystep * i for i in range(precision)]

    values = []
    for y in ylist:
        for x in xlist:
            values.append(problem.evaluate([x, y]))

    # max_val = max(values)
    # lim = 5
    # values = [v if v < lim else lim + (v / max_val) for v in values]
    max_val = max(values)
    s = int(np.sqrt(len(values)))
    values = np.reshape(values, (s, s))

    plt.figure(figsize=(11, 8))

    ax = sbn.heatmap(values, linewidths=0.0, cbar=True, cmap="coolwarm",
                     xticklabels=100, yticklabels=100, vmin=0, vmax=max_val,
                     alpha=0.8)

    cbar = ax.collections[0].colorbar
    cbar.set_ticks([0, max_val])
    cbar.set_ticklabels(["Global Min", "Gloabl Max"])
    cbar.ax.tick_params(labelsize=16)

    xstep = xrange / 10
    ystep = yrange / 10

    ax.set_xticklabels([round(xmin + i * xstep, 2) for i in range(11)], fontsize=14)
    ax.set_yticklabels([round(ymin + i * ystep, 2) for i in range(11)], rotation=0, fontsize=14)
    ax.set_xlabel("X", fontsize=16)
    ax.set_ylabel("Y", fontsize=16, rotation=0)

    title = problem.get_name()
    ax.set_title(title.capitalize(), fontsize=18)
    ax.invert_yaxis()


    x, y = draw_global_minimum(prob)
    ax.scatter(x, y, marker="*", s=100, color="yellow")

    if do_save_img:
        title = title.replace(".", "")
        plt.savefig(f"../images/heatmaps/{title}")
    return ax


def draw_global_minimum(prob):
    xrange = abs(prob.get_min_values()[0] - prob.get_max_values()[0])
    yrange = abs(prob.get_min_values()[1] - prob.get_max_values()[1])
    xpos = (abs(prob.get_min_values()[0] - prob.get_optimum_position()[0]) / xrange) * 1002
    ypos = (abs(prob.get_min_values()[1] - prob.get_optimum_position()[1]) / yrange) * 1002
    return xpos, ypos


is_saving = True

if __name__ == "__main__":
    for prob in problems.get_problems().values():
        ax = create_heatmap_array(prob, do_save_img=is_saving)

        plt.show(block=not is_saving)
        plt.close()
