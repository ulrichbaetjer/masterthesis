import matplotlib.pyplot as plt

from matplotlib.patches import Polygon
import matplotlib as mpl
import masterthesis.visualization.transform as transform
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from masterthesis.pso.vehicles.reed_shepp_particle import ReedsSheppParticle
from masterthesis.pso.vehicles.bezier_particle import BezierParticle
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.pso.swarm import Swarm

import math

start_pos = (0, 0, 0)
goal_1 = [0.75, 1.25, 0]           # 1.25
goal_2 = [1, 0, 0.0]


axis_ranges = (-1.1, 3.9, -1.1, 3.9)

scale = 20

x_distance = abs(axis_ranges[0] - axis_ranges[1]) / scale
y_distance = abs(axis_ranges[2] - axis_ranges[3]) / (scale * 5)

alpha_val = 0.8

travelled_a = 0
travelled_b = 0
travelled_c = 0


def setup_plot(axis=axis_ranges, xlabel="X-Axis", ylabel="Y-Axis"):
    plt.axis(axis)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    fig = plt.gcf()

    fig.set_size_inches(10, 10)


def swap_positions(p):
    if p.position[:-1] == goal_1[:-1]:
        p.previous_position = p.position
        copy_goal2 = [n for n in goal_2]
        p.position = copy_goal2


def get_optimal_degree(p):
    min_length = math.inf
    best_degree = math.inf
    rad_convert = (math.pi / 180)
    copy_goal2 = list(goal_2)
    for degree in range(360):
        rad = degree * rad_convert
        p.position[-1] = rad
        path1 = p.create_path(p.previous_position, p.position, p.turn_radius)
        copy_goal2[-1] = p.find_next_best_orientation(p.position, copy_goal2)
        path2 = p.create_path(p.position, copy_goal2, p.turn_radius)
        achieved_length = p.get_path_length(path1) + p.get_path_length(path2)
        # print("length: ", achieved_length, " at: ", rad)
        if achieved_length < min_length:
            best_degree = rad
            min_length = achieved_length
    # print("best radian: ", best_degree)
    return best_degree


colors = ["blue", "green", "red"]


def split_sample(p):
    sample = p.get_path_sample(p.path)
    x_samples = [s[0] for s in sample]
    y_samples = [s[1] for s in sample]
    return x_samples, y_samples


# draw three vehicles with the optimal path, the NU policy and GRDY policy
def draw_swarm(swarm):
    setup_plot()
    ax = plt.gca()

    for it in range(2):
        col_it = iter(colors)
        for p in swarm.particles:
            col = next(col_it)
            start = get_polygon(ax, p.previous_position, col, 1)

            if p.orientation_style == OrientationEnums.greedy:
                p.position[-1] = p.find_next_best_orientation(p.previous_position, p.position)
            if p.orientation_style == "special":
                if p.position[:-1] == goal_1[:-1]:
                    p.position[-1] = get_optimal_degree(p)
                else:
                    p.position[-1] = p.find_next_best_orientation(p.previous_position, p.position)

            finish = get_polygon(ax, p.position, col, 1)
            if it == 0:
                ax.add_patch(start)
            ax.add_patch(finish)
            p.path = p.create_path(p.previous_position, p.position, p.turn_radius)

            if col == "blue":
                global travelled_a
                travelled_a += p.get_path_length(p.path)
                # print("blue: ", travelled_a)
            elif col == "green":
                global travelled_b
                travelled_b += p.get_path_length(p.path)
                # print("green: ", travelled_b)
            elif col == "red":
                global travelled_c
                travelled_c += p.get_path_length(p.path)
                # print("red: ", travelled_c)

            x_samples, y_samples = split_sample(p)
            alpha = 0.8 if col == "blue" else 0.8
            style = "--" if col == "blue" else "--"
            dash = (4, 2) if col == "blue" else (4, 4)

            leg = None
            if col == "blue":
                leg = "No Update"
            elif col == "green":
                leg = "Greedy"
            elif col == "red":
                leg = "Optimal"

            if it == 0:
                ax.plot(x_samples, y_samples, col, alpha=alpha, linestyle=style, dashes=dash, label=leg)
            else:
                ax.plot(x_samples, y_samples, col, alpha=alpha, linestyle=style, dashes=dash)
            swap_positions(p)

    mark_points(ax)

    precision = 3
    fsize = 20

    trav_str_a = format(round(travelled_a, precision), ".3f")
    trav_str_a = " " * (6 - len(trav_str_a)) + trav_str_a
    trav_str_b = format(round(travelled_b, precision), ".3f")
    trav_str_b = " " * (6 - len(trav_str_b)) + trav_str_b
    trav_str_c = format(round(travelled_c, precision), ".3f")
    trav_str_c = " " * (6 - len(trav_str_c)) + trav_str_c # len(trav_str_a)

    delta = 0.03125
    ax.annotate(" Blue travelled: " + trav_str_a, (axis_ranges[1] - 2.5, axis_ranges[3] - 0.25), fontName="monospace", fontSize=fsize)
    ax.annotate("Green travelled: " + trav_str_b, (axis_ranges[1] - 2.5, axis_ranges[3] - delta*6 - 0.25), fontName="monospace", fontSize=fsize)
    ax.annotate("  Red travelled: " + trav_str_c, (axis_ranges[1] - 2.5, axis_ranges[3] - delta * 12 - 0.25), fontName="monospace", fontSize=fsize)

    ax.legend(loc="upper left")

    plt.xlabel("X-Axis", fontsize=15)
    plt.ylabel("Y-Axis", fontsize=15)
    plt.show()


def mark_points(ax):
    point_size = 15
    letter_size = 18
    ax.plot(goal_1[0], goal_1[1], "ro", markersize=point_size)
    ax.plot(start_pos[0], start_pos[1], "ro", markersize=point_size)
    ax.plot(goal_2[0], goal_2[1], "ro", markersize=point_size)

    delta = 0.03125
    ax.annotate("A", (start_pos[0] + delta, start_pos[1] + delta), fontSize=letter_size)
    ax.annotate("B", (goal_1[0] + delta, goal_1[1] - delta), fontSize=letter_size)
    ax.annotate("C", (goal_2[0] + delta, goal_2[1] + delta), fontSize=letter_size)


def annotate_location(ax, x, y, text):
    ax.annotate(text, xy=(x, y), fontSize=14)


def get_polygon(ax, pos, col, scaling):

    extra = 0

    if pos == start_pos and col == "blue":
        extra = 2**-5
    elif pos == start_pos and col == "red":
        extra = 0.0000001
    elif pos == start_pos and col == "green":
        extra = 0.015

    points = [[pos[0], pos[1]],
              [pos[0] - (x_distance / scaling) - extra, pos[1] + (y_distance / scaling) + extra],
              [pos[0] - (x_distance / scaling) - extra, pos[1] - (y_distance / scaling) - extra]]

    t = transform.get_transformation(ax, pos)
    polygon = Polygon(points, fc=col, transform=t, alpha=alpha_val, ec="black")
    return polygon


def setup_example():
    plt.axis((-1, 3, -1, 3))
    plt.axis("off")
    fig = plt.gcf()

    fig.set_size_inches(10, 10)


def example():
    setup_example()

    pos_a1 = [n for n in goal_1]
    pos_a1[-1] = math.pi

    pos_a2 = [n for n in goal_1]
    pos_a2[-1] = (3*math.pi) / 2

    pos_b1 = [n for n in goal_2]
    pos_b1[-1] = 0

    pos_b2 = [n for n in goal_2]
    pos_b2[-1] = (3*math.pi) / 2

    car1_color = "blue"
    car2_color = "green"
    draw_example_path(pos_a1, pos_b1, car1_color)
    ax = plt.gca()
    annotate_location(ax, 1.2, -0.55, "Path 1a")
    draw_example_path(pos_a2, pos_b2, car2_color)
    annotate_location(ax, 1.6, 2.3, "Path 2a")

    draw_example_polygons(pos_a1, pos_b1, car1_color)
    annotate_location(ax, -0.85, 0.4, "Path 1b")
    draw_example_polygons(pos_a2, pos_b2, car2_color)
    annotate_location(ax, 0.88, 0.85, "Path 2b")

    mark_points(plt.gca())
    plt.tight_layout()
    plt.show()


def draw_example_polygons(pos1, pos2, col):
    ax = plt.gca()
    start_scale = 0.8 if col == "blue" else 1.2
    start = get_polygon(ax, [0, 0, 0], col, start_scale)
    checkpoint = get_polygon(ax, pos1, col, 1.0)
    end = get_polygon(ax, pos2, col, 1.0)

    ax.add_patch(start)
    ax.add_patch(checkpoint)
    ax.add_patch(end)


def draw_example_path(pos1, pos2, col):
    car = DubinsParticle(pos=[0, 0, 0])
    car.previous_position = car.position
    car.position = pos1

    car.path = car.create_path(car.previous_position, car.position, 1.0)
    sa_xs, sa_ys = split_sample(car)

    car.orientation_style = OrientationEnums.greedy
    car.update_orientation(pos2)

    car.previous_position = car.position
    car.position = pos2

    car.path = car.create_path(car.previous_position, car.position, 1.0)
    ab_xs, ab_ys = split_sample(car)

    plt.plot(sa_xs, sa_ys, col, alpha=0.5, linestyle="--")
    plt.plot(ab_xs, ab_ys, col, alpha=0.5, linestyle="--")


# setup the different positions
def dubins_setup():
    pos1 = [n for n in goal_1]
    pos2 = [n for n in goal_1]
    pos3 = [n for n in goal_1]

    particle1 = DubinsParticle(pos1, 3)

    v = VehicleDescription(orientation_style=OrientationEnums.greedy)
    particle2 = DubinsParticle(pos2, 3, vehicle_description=v)
    particle3 = DubinsParticle(pos3, 3)

    particle1.previous_position = start_pos
    particle2.previous_position = start_pos
    particle3.previous_position = start_pos

    swarm = Swarm([particle1, particle2, particle3], 3)
    particle3.orientation_style = "special"

    return swarm


def reedsshepp_setup():
    pos1 = [n for n in goal_1]
    pos2 = [n for n in goal_1]
    pos3 = [n for n in goal_1]

    particle1 = ReedsSheppParticle(pos1, 3)

    v = VehicleDescription(orientation_style=OrientationEnums.greedy)
    particle2 = ReedsSheppParticle(pos2, 3, vehicle_description=v)

    particle3 = ReedsSheppParticle(pos3, 3)
    particle3.orientation_style = "special"

    particle1.previous_position = start_pos
    particle2.previous_position = start_pos
    particle3.previous_position = start_pos
    swarm = Swarm([particle1, particle2, particle3], 3)

    return swarm


def bezier_setup():
    pos1 = [n for n in goal_1]
    pos2 = [n for n in goal_1]
    pos3 = [n for n in goal_1]

    particle1 = BezierParticle(pos1, 3)

    v = VehicleDescription(orientation_style=OrientationEnums.greedy)
    particle2 = BezierParticle(pos2, 3, vehicle_description=v)

    particle3 = BezierParticle(pos3, 3)

    particle3.orientation_style = "special"

    particle1.previous_position = start_pos
    particle2.previous_position = start_pos
    particle3.previous_position = start_pos
    swarm = Swarm([particle1, particle2, particle3], 3)

    return swarm


def reset_travel_distance():
    global travelled_a, travelled_b, travelled_c
    travelled_a, travelled_b, travelled_c = 0, 0, 0


demonstrate_policy_difference = True

if __name__ == "__main__":

    if demonstrate_policy_difference:
        s = dubins_setup()
        draw_swarm(s)

        reset_travel_distance()

        s = reedsshepp_setup()
        draw_swarm(s)

        reset_travel_distance()

        s = bezier_setup()
        draw_swarm(s)
    else:
        example()


