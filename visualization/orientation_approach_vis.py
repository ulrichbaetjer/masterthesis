import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


from masterthesis.pso.swarm import Swarm
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.pso.pso_enums import PsoEnums

from masterthesis.pso.vehicles.bezier_particle import BezierParticle
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from masterthesis.pso.vehicles.straight_particle import StraightParticle
from masterthesis.pso.vehicles.reed_shepp_particle import ReedsSheppParticle

import masterthesis.visualization.transform as transform
import masterthesis.optimization_function.problem_collection as problems
import math
import random


colors = ["#800000", "#111111", "#469990", "#911eb4", "#f032e6",
          "#660000", "#990000", "#CC0000", "#FF0000", "#FF3333"]

axis_ranges = [-6, 6, -6, 6]

scale = 20
alpha = 0.75

x_distance = abs(axis_ranges[0] - axis_ranges[1]) / scale
y_distance = abs(axis_ranges[2] - axis_ranges[3]) / (scale * 5)


def setup_plot(axis=axis_ranges, xlabel="X-Axis", ylabel="Y-Axis"):
    plt.axis(axis)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    fig = plt.gcf()
    fig.set_size_inches(10, 10)


def draw_swarm(swarm, best_pos):
    setup_plot()
    ax = plt.gca()

    average_distance = 0

    line_colors = iter(colors)

    # only show a few particles instead of all
    filtered_swarm_particles = ([p for p in swarm.particles if p.personal_best[:-1] == swarm.best_found_position[:-1]])
    filtered_swarm_particles.append(swarm.particles[0])
    filtered_swarm_particles.append(swarm.particles[1])
    filtered_swarm_particles.append(swarm.particles[5])

    # filtered_swarm_particles = swarm.particles

    for p in filtered_swarm_particles:
        col = next(line_colors)
        start = get_polygon(ax, p.previous_position, col, 1.5, 1)
        finish = get_polygon(ax, p.position, col, 1, alpha)
        ax.add_patch(start)
        ax.add_patch(finish)

        sample = p.get_path_sample(p.path)
        average_distance += p.get_path_length(p.path)
        x_samples = [s[0] for s in sample]
        y_samples = [s[1] for s in sample]
        ax.plot(x_samples, y_samples, col, alpha=0.7)

        p_point_x = p.position[0]
        p_point_y = p.position[1]
        ax.scatter(p_point_x, p_point_y, color=col, marker=".")

    average_distance /= swarm.size
    swarm_center = swarm.calculate_center_of_swarm()
    best_particle_pos = swarm.best_found_position[:-1]
    ax.plot(swarm_center[0], swarm_center[1], "bo", alpha=1)
    ax.plot(best_particle_pos[0], best_particle_pos[1], "go", alpha=1)

    info_about_travelled_dist = "Average travelled distance: " + str(round(average_distance, 5))
    ax.text(axis_ranges[1] - 4.8, axis_ranges[3] - 0.375, info_about_travelled_dist, fontsize=12)
    title = get_title(swarm)
    ax.set_title(title.capitalize())
    plt.show(block=False)
    plt.savefig(f"../images/approaches/{swarm.swarm_type.name}/{swarm.particles[0].orientation_style.name}")
    plt.close()


def get_title(swarm):
    if swarm.swarm_type == PsoEnums.straight:
        return str(swarm.swarm_type.name)
    else:
        return str(swarm.swarm_type.name) + " with " + str(swarm.particles[0].orientation_style.name) + " policy"


def get_polygon(ax, pos, col, scaling, alpha_level):

    points = [[pos[0], pos[1]], [pos[0] - (x_distance / scaling), pos[1] + (y_distance / scaling)],
              [pos[0] - (x_distance / scaling), pos[1] - (y_distance / scaling)]]

    t = transform.get_transformation(ax, pos)

    polygon = Polygon(points, fc=col, transform=t, edgecolor="black", alpha=alpha_level)
    return polygon


def setup_swarms():

    optimize = problems.get_problems()["ackley"]
    best_pos = optimize.get_optimum_position()
    positions = [[-4, -4], [-4, -2],
                 [-2, -2], [-2, 0],
                 [0, 3], [0, 4],
                 [2, 2], [2, 4],
                 [4, 2], [4, 4]]

    frac = math.tau / len(positions)

    for idx, po in enumerate(positions):
        po.append(frac * idx)

    swarms = []
    for particle_class, pso_enum in [(StraightParticle, PsoEnums.straight),
                                     (DubinsParticle, PsoEnums.dubins),
                                     (ReedsSheppParticle, PsoEnums.reeds_shepp),
                                     (BezierParticle, PsoEnums.bezier)
                                    ]:
        for orientation_style in OrientationEnums:
            particles = []
            rand = random.Random(19941208)
            for pos in positions:
                copy_of_pos = [v for v in pos]
                particles.append(particle_class(copy_of_pos,
                                                inertia=0.5,
                                                vehicle_description=VehicleDescription(orientation_style=orientation_style),
                                                rand=rand))
            random.seed(19941208)
            swarm_ = Swarm(particles, len(particles), optimize, pso_enum,
                           randomness=rand)

            swarm_.update_swarm()
            swarm_.update_swarm()
            # swarm_.update_swarm()

            swarms.append(swarm_)
            if particle_class == StraightParticle:
                break

    for s in swarms:
        draw_swarm(s, best_pos)


if __name__ == "__main__":

    setup_swarms()
