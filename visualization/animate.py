import math
import random

import matplotlib.pyplot as plt
from matplotlib import rcParams

from masterthesis.pso.vehicles.straight_particle import StraightParticle
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from masterthesis.pso.vehicles.bezier_particle import BezierParticle
from masterthesis.pso.vehicles.reed_shepp_particle import ReedsSheppParticle
from masterthesis.pso.vehicles.rtr_particle import RTRParticle

import masterthesis.optimization_function.problem_collection as problems
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.swarm import Swarm
from masterthesis.pso.pso_enums import PsoEnums
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription

import masterthesis.visualization.map as map


dict_vehicles = {
    PsoEnums.straight: StraightParticle,
    PsoEnums.dubins: DubinsParticle,
    PsoEnums.reeds_shepp: ReedsSheppParticle,
    PsoEnums.bezier: BezierParticle,
    PsoEnums.rtr: RTRParticle
}


def show_approaches(model):

    frac = math.tau / 10
    optimize = problems.get_problems()["ackley"]
    best_pos = optimize.get_optimum_position()
    positions = [[-1.5, -1.5, 0], [1, -0.5, frac], [0, 1, frac*2], [-1.5, 0, frac * 3],
                 [-1.5, 1.5, frac*4], [0, -1.5, frac*5], [0, 1.5, frac*6], [1.5, -1.5, frac*7],
                 [1.5, 0, frac*8], [1.5, 1.5, frac*9]]

    copy_of_pos = []
    for pos in positions:
        part = [x for x in pos]
        copy_of_pos.append(part)

    vehicle_class = dict_vehicles[model]

    if model == PsoEnums.straight:
        setup_animation(model.name, vehicle_class, copy_of_pos, None, best_pos, optimize)

    else:
        for orient in OrientationEnums:
            copy_of_pos = []
            for pos in positions:
                part = [x for x in pos]
                copy_of_pos.append(part)

            setup_animation(model.name + orient.name, vehicle_class, copy_of_pos, orient, best_pos, optimize)


def setup_animation(title, particle_class, positions, orientation_style, best_pos, optimize):
    particles = []
    for idx in range(len(positions)):
        particles.append(particle_class(positions[idx], inertia=0.5,
                                        vehicle_description=VehicleDescription(orientation_style=orientation_style), rand=random.Random(19941208)))

    random.seed(19941208)
    _swarm = Swarm(particles, len(particles), optimize, PsoEnums.dubins,
                         randomness=random.Random(19941208))
    animate(title, _swarm, best_pos)


def animate(title, swarm, best_pos):
    fig = plt.figure()
    fig.canvas.set_window_title(title)
    # map.draw_swarm(swarm, 0, best_pos)
    plt.ioff()
    # for it in range(3):
    #     swarm.update_swarm()
    #     plt.clf()
    #     map.draw_vehicle_movement(fig, swarm, it, best_pos)

    a = map.PathAnimation(fig, swarm, 5, 200, [fig, best_pos])
    plt.draw()
    plt.show(block=False)

    rcParams['animation.ffmpeg_path'] = r"/usr/bin/ffmpeg"
    a.ani.save(r"/home/ulrich/Schreibtisch/Uni/Masterarbeit/PythonPrototype/" + title + ".mp4", writer="ffmpeg")

    print(str(swarm.particles[0].orientation_style) + ": " + str(swarm.get_average_distance_travelled()))


if __name__ == "__main__":
    for vehicle in PsoEnums:
        show_approaches(vehicle)
