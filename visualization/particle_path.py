from masterthesis.pso import particle
from masterthesis.pso.vehicles import vehicle_particle


class ParticlePath:
    def __init__(self, p):
        self.index = 0
        self.max_index = -1
        self.path = []
        self.is_done = False

        if type(p) is particle.Particle:
            self.__sample_path(p.previous_position, p.position, 10)

        elif isinstance(p, vehicle_particle.VehicleParticle):
            if p.path is None:
                self.path.append(p.position)
            else:
                self.path = p.get_path_sample(p.path)
                if not self.path or self.path[-1] != p.position:
                    self.path.append(p.position)

            self.max_index = len(self.path)

        else:
            raise ValueError("Particle Path needs a specific Particle type to work")

    def iterate(self):
        coordinates = self.path[self.index]
        if self.index < (self.max_index - 1):
            self.index += 1
        else:
            self.is_done = True
        return coordinates

    def line_slice(self):
        return self.path[0:self.index]

    def __sample_path(self, start, end, sample_size):
        distance = ((end[0] - start[0]) / sample_size, (end[1] - start[1]) / sample_size)
        self.path = []
        for step in range(sample_size + 1):
            self.path.append(((start[0] + distance[0]) * step, (start[1] + distance[1]) * step, 0))

    def reset_index(self):
        self.index = 0
        self.is_done = False
