# this file analyzes the spectrum of possible path lengths for a model that drives from [X1, Y1, Theta] to [X2, Y2, _]
import dubins
import reeds_shepp
import bezier
import masterthesis.helper_functions.bezier_functions as bezier_helper
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import math
import numpy as np


RADIAN_INDEX = 2
path_to_save = "../images/path_spectrum/path_analysis/"
id = 1


def create_dubins_path_length(a, b, r):
    return dubins.shortest_path(a, b, r).path_length()


def create_reeds_shepp_length(a, b, r):
    return reeds_shepp.PyReedsSheppPath(a, b, r).distance()


def create_bezier_length(a, b, r):
    return bezier.curve.Curve(bezier_helper.calculate_control_points(a, b, r), degree=3).length


def get_model_name(model):
    if model == create_dubins_path_length:
        return "dubins model"
    elif model == create_reeds_shepp_length:
        return "reeds shepp model"
    elif model == create_bezier_length:
        return "bezier model"
    else:
        return "unknown model"


def get_spectrum_data(point_a, point_b, radius, model):

    distances = []
    angles = []

    degree = 0
    step = 0.25
    while degree <= 360:
        rad = degree * (math.pi / 180)
        point_b[RADIAN_INDEX] = rad
        length = model(point_a, point_b, radius)
        distances.append(length)
        angles.append(degree)
        degree += step

    return distances, angles


def get_error_data(point_a, point_b, radius, model):
    distances = []
    angles = []

    degree = 0
    step = 0.25
    while degree <= 360:
        rad = degree * (math.pi / 180)
        point_a[RADIAN_INDEX] = rad
        point_b[RADIAN_INDEX] = rad
        length = model(point_a, point_b, radius)
        distances.append(length)
        angles.append(degree)
        degree += step

    return distances, angles


def analysis(point_a, point_b, radius, model, spectrum=get_spectrum_data):
    fig = plt.figure(num=None, figsize=(10, 6), dpi=120, facecolor="w", edgecolor="b")

    distances, angles = spectrum(point_a, point_b, radius, model)

    buffer = 0.075
    max_distance = max(distances) + buffer
    min_distance = min(distances) - buffer
    delta = max_distance - min_distance
    number_of_ticks = 9
    step = delta / number_of_ticks
    yticks = [min_distance + (step * tick)for tick in range(number_of_ticks+1)]

    ax = plt.gca()
    point_b[-1] = '?'
    tmp = point_a[-1]
    point_a[-1] = "90°"
    plt.title(f"{get_model_name(model)} same orientation for both points: from {point_a} to {point_b}".title())
    point_a[-1] = tmp
    ax.set_ylabel("Path Length")
    ax.set_xlabel("Goal Orientation In Degree")
    ax.plot(angles, distances)
    ax.set_xticks([x for x in range(0, 361, 20)])
    ax.set_yticks(yticks)
    ax.set_xlim([0, 360])
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))

    ax.axis([0, 360, min_distance, max_distance])
    fig.add_axes(ax)
    return fig


def show_error():
    err_fig1 = analysis([0, 0, 0], [0, 0, 0], 0.5, create_dubins_path_length, spectrum=get_error_data)
    a = err_fig1.axes
    a[0].set_title("Dubins Model Path Lengths From (0, 0, X) To (0, 0, X)")
    # a[0].set_xlim([0, 81]) # interesting range
    plt.show()
    plt.close(err_fig1)

    err_fig2 = analysis([0, 0, 0], [0, 0, 0], 1.0, create_reeds_shepp_length, spectrum=get_error_data)
    b = err_fig2.axes
    b[0].set_title("Reeds Shepp Model Path Lengths From (0, 0, X) To (0, 0, X)")
    plt.show()
    plt.close(err_fig2)

    err_fig3 = analysis([0, 0, 0], [0, 0, 0], 1.0, create_bezier_length, spectrum=get_error_data)
    c = err_fig3.axes
    c[0].set_title("Bezier Curve Model Path Lengths From (0, 0, X) To (0, 0, X)")
    plt.show()
    plt.close(err_fig3)


def show_model(model):
    global id
    id = 1
    radius = 1
    z = 0.0
    point_a = [1.0, 1.0, math.pi/2]
    points_b = [ [0.0, 0.0, z], [0.0, 0.5, z], [0.0, 1.0, z], [0.0, 1.5, z], [0.0, 2.0, z],
                 [0.5, 0.0, z], [0.5, 0.5, z], [0.5, 1.0, z], [0.5, 1.5, z], [0.5, 2.0, z],
                 [1.0, 0.0, z], [1.0, 0.5, z], [1.0, 1.0, z], [1.0, 1.5, z], [1.0, 2.0, z],
                 [1.5, 0.0, z], [1.5, 0.5, z], [1.5, 1.0, z], [1.5, 1.5, z], [1.5, 2.0, z],
                 [2.0, 0.0, z], [2.0, 0.5, z], [2.0, 1.0, z], [2.0, 1.5, z], [2.0, 2.0, z]
               ]

    for point_b in points_b:
        fig = analysis(point_a, point_b, radius, model)
        folder = get_model_name(model).replace(' ', '_')
        fig.savefig(path_to_save + f"{folder}/{folder}_spectrum_{id}.png")
        id += 1
        plt.close(fig)


meh = False
if __name__ == "__main__":

    show_error()

    if meh:
        print("Start with dubins")
        show_model(create_dubins_path_length)
        print("Start with reeds shepp")
        show_model(create_reeds_shepp_length)
        print("Start with bezier")
        show_model(create_bezier_length)
        print("Done")
