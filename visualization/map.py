import matplotlib.pyplot as plt
from matplotlib.patches import Polygon, Arrow
import matplotlib as mpl
import matplotlib.animation as animation

import math
import random

from masterthesis.optimization_function.ackley_func import AckleyOptimization
from masterthesis.pso.swarm import Swarm

framerate = 1.0 / 60.0

axis_ranges = [-6, 6, -6, 6]

scale = 20

x_distance = abs(axis_ranges[0] - axis_ranges[1]) / scale
y_distance = abs(axis_ranges[2] - axis_ranges[3]) / (scale * 5)


def setup_plot(axis=axis_ranges, xlabel="X-Axis", ylabel="Y-Axis"):
    plt.axis(axis)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    fig = plt.gcf()
    fig.set_size_inches(11, 8)


colors = ["#660066", "#990099", "#CC00CC", "#FF00FF", "#FF33FF",
          "#660000", "#990000", "#CC0000", "#FF0000", "#FF3333"]


def draw_swarm(swarm, iteration, known_optimum):
    setup_plot()
    x_positions, y_positions = swarm.get_swarm_positions()
    center = swarm.calculate_center_of_swarm()

    plt.plot(x_positions, y_positions, "ro")
    plt.plot(center[0], center[1], "bo")

    plt.plot(known_optimum[0], known_optimum[0], "go")

    plt.title("Current iteration: " + str(iteration))
    plt.draw()
    plt.pause(framerate)


class PathAnimation:
    def __init__(self, fig, swarm, max_iterations, frames, args):
        self.swarm = swarm
        self.swarm.update_swarm()
        self.paths = self.swarm.get_swarm_movement()
        self.current_iteration = 0
        self. max_iterations = max_iterations
        self.max_frames = frames
        self.is_done = False
        self.ani = animation.FuncAnimation(fig, self.animate, fargs=args, interval=48, repeat=True, frames=self.max_frames)

    def animate(self, i, fig, known_optimum):
        if self.current_iteration >= self.max_iterations:
            setup_plot()
            fig.gca().set_title("Done")

            if not self.is_done:
                print("Done")

            self.is_done = True
            return

        setup_plot()

        if has_still_movements(self.paths):
            fig.clf()
            setup_plot()
            ax = fig.gca()

            line_colors = iter(colors)

            for p in self.paths:
                pos = p.iterate()
                points = [[pos[0], pos[1]], [pos[0] - x_distance, pos[1] + y_distance],
                          [pos[0] - x_distance, pos[1] - y_distance]]

                x = pos[0]
                y = pos[1]
                degree = (pos[2] * 180) / math.pi

                color = next(line_colors, None)
                if color is None:
                    line_colors = iter(colors)
                    color = next(line_colors)

                # https://stackoverflow.com/questions/15557608/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-a
                # https://stackoverflow.com/questions/43000288/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-d
                ts = ax.transData
                # coords = ts.transform([x, y])
                # t = ts + tr
                coords = [x, y]
                tr = mpl.transforms.Affine2D().rotate_deg_around(coords[0], coords[1], degree)
                t = tr + ts

                polygon = Polygon(points, fc=color, transform=t, alpha=0.9)
                ax.add_patch(polygon)

                line_points = p.line_slice()
                # A line needs 2 points to work
                if len(line_points) > 1:
                    line_x, line_y, _ = zip(*line_points)
                    line = plt.Line2D(line_x, line_y, color=color, alpha=0.7)
                    ax.add_line(line)

            # ax.plot(center[0], center[1], "b.")
            ax.plot(known_optimum[0], known_optimum[0], "g.")

            ax.set_title("Current iteration: " + str(self.current_iteration))
        else:
            self.swarm.update_swarm()
            self.paths = self.swarm.get_swarm_movement()
            self.current_iteration += 1


def draw_vehicle_movement(fig, swarm, iteration, known_optimum):
    setup_plot()
    paths = swarm.get_swarm_movement()
    center = swarm.calculate_center_of_swarm()

    while has_still_movements(paths):
        fig.clf()
        setup_plot()
        ax = fig.gca()

        line_colors = iter(colors)
        for p in paths:
            pos = p.iterate()
            points = [[pos[0], pos[1]], [pos[0]-x_distance, pos[1]+y_distance], [pos[0]-x_distance, pos[1]-y_distance]]

            x = pos[0]
            y = pos[1]
            degree = (pos[2] * 180) / math.pi

            # https://stackoverflow.com/questions/15557608/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-a
            # https://stackoverflow.com/questions/43000288/unable-to-rotate-a-matplotlib-patch-object-about-a-specific-point-using-rotate-d
            ts = ax.transData
            # coords = ts.transform([x, y])
            # t = ts + tr
            coords = [x, y]
            tr = mpl.transforms.Affine2D().rotate_deg_around(coords[0], coords[1], degree)
            t = tr + ts

            polygon = Polygon(points, fc="red", transform=t)
            ax.add_patch(polygon)

            line_points = p.line_slice()
            # A line needs 2 points to work
            if len(line_points) > 1:
                line_x, line_y, _ = zip(*line_points)
                line = plt.Line2D(line_x, line_y, color=next(line_colors))
                ax.add_line(line)

        ax.plot(center[0], center[1], "b.")
        ax.plot(known_optimum[0], known_optimum[0], "g.")

        fig.title = "Current iteration: " + str(iteration)

        plt.pause(framerate)


def has_still_movements(paths):
    is_active = False
    for p in paths:
        if not p.is_done:
            is_active = True
            break
    return is_active


def test_triangle():
    points = [[0, 0], [-1, -0.5], [-1, 0.5]]
    p = Polygon(points, closed=True, fc="red")

    axis = plt.gca()
    t = mpl.transforms.Affine2D().rotate_deg(0) + axis.transData
    p.set_transform(t)
    axis.add_patch(p)
    axis.set_xlim(-5, 5)
    axis.set_ylim(-5, 5)
    plt.show()


if __name__ == "__main__":
    test_triangle()
