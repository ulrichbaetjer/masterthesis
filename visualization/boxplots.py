import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import masterthesis.dataframes.pandas_settings as pd_options
from masterthesis.optimization_function.problem_collection import get_problems
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.pso_enums import PsoEnums
import masterthesis.visualization.io_result_files as rw_files


def test():
    np.random.seed(2345)
    df = pd.DataFrame(np.random.randn(20, 4),
                      columns=['C1', 'C2', 'C3', 'C4'])
    boxplot = df.boxplot(column=['C1', 'C2', 'C3'])
    plt.show()


def draw_policies_boxplots(dfs, title, opt):
    l = []
    orients = []
    model = dfs[0][pd_options.dict_model][0]
    for df in dfs:

        if model == PsoEnums.straight.name and len(orients) == 0:
            orientation = "No Scan"
        elif model == PsoEnums.straight.name and len(orients) == 1:
            orientation = "With Scan"

        else:
            orientation = rw_files.dict_long_names[df[pd_options.dict_orientation][0]]

        orients.append(orientation)
        x = df.loc[df[pd_options.dict_generation] == 300]
        l.append(x[opt].tolist())

    m = list(map(list, zip(*l)))
    dataframe = pd.DataFrame(m, columns=orients)

    boxplot = dataframe.boxplot()
    plt.title(title.title(), fontsize=16)
    plt.xlabel("", fontsize=14)
    plt.ylabel(rw_files.stylize_option(opt, model), fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    plt.gcf().set_size_inches((9, 6))
    plt.tight_layout()
    plt.show(block=not is_saving)


def draw_policy_over_time(df, title, opt):
    l = []
    gens = []
    model = df[pd_options.dict_model][0]
    for i in range(1, 31):
        gen = 10 * i
        x = df.loc[df[pd_options.dict_generation] == gen]
        l.append(x[opt].tolist())
        gens.append(gen)

    m = list(map(list, zip(*l)))
    dataframe = pd.DataFrame(m, columns=gens)
    boxplot = dataframe.boxplot()
    plt.title(title, fontsize=16)
    plt.xlabel("Generation", fontsize=14)
    plt.ylabel(rw_files.stylize_option(opt, model), fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.gcf().set_size_inches((11, 8))

    if opt == pd_options.dict_fit:
        plt.yscale("log")

    for label in plt.gca().xaxis.get_ticklabels()[::2]:
        label.set_visible(False)

    plt.tight_layout()
    plt.show(block=not is_saving)


def setup_policy_plot(seed, model, problem, scan, opt, orientation):
    if model == PsoEnums.straight.name:
        orientation = OrientationEnums.straight_model_policy

    o = str(orientation)
    dfs = rw_files.get_dataframes(seed, model, problem, scan)
    df = None
    for d in dfs:
        if d[pd_options.dict_orientation][0] == o:
            df = d
            break
    title = rw_files.stylize_title(model, problem, scan, orientation)

    draw_policy_over_time(df, title, opt)
    if is_saving:
        path = rw_files.create_path("boxplots/single", seed, model, "Median", scan, opt)
        rw_files.save_image(plt.gcf(), path, f"{problem.replace('.', '')}_{scan}")


def setup_policies_plot(seed, model, problem, scan, opt):
    if model == PsoEnums.straight.name:
        dfs = rw_files.get_dataframes(seed, model, problem, "no_scan")
        tmp = rw_files.get_dataframes(seed, model, problem, "with_scan")
        dfs.append(tmp[0])

    else:
        dfs = rw_files.get_dataframes(seed, model, problem, scan)

    clean_scan = "w. scan" if scan == "with_scan" else "w/o scan"
    if model == PsoEnums.straight.name:
        title = f"{model.replace('_', ' ')} model {problem}".capitalize()
    else:
        title = f"{model.replace('_', ' ')} model {problem} {clean_scan}".capitalize()
    draw_policies_boxplots(dfs, title, opt)
    if is_saving:
        path = rw_files.create_path("boxplots/multi", seed, model, "Median", scan, opt)
        rw_files.save_image(plt.gcf(), path, f"{problem.replace('.', '')}_{scan}")


def main():
    pd_options.setup_dataframe()
    seed_id = 1609774586915
    # opt = pd_options.dict_avg_dist
    # opt = pd_options.dict_avg_path_len
    # opt = pd_options.dict_longest_path_len
    # opt = pd_options.dict_fit
    for opt in [pd_options.dict_avg_dist, pd_options.dict_avg_path_len, pd_options.dict_longest_path_len, pd_options.dict_fit]:
        for model in PsoEnums:
            for problem in get_problems():
                for scan in ["no_scan", "with_scan"]:
                    setup_policies_plot(seed_id, model.name, problem, scan, opt)
                    plt.clf()
                    setup_policy_plot(seed_id, model.name, problem, scan, opt, OrientationEnums.greedy)
                    plt.clf()
    print("done")


is_saving = True

if __name__ == "__main__":
    main()

