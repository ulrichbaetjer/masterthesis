# This file was used for an older performance visualization
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import masterthesis.dataframes.pandas_settings as pd_settings
import masterthesis.optimization_function.problem_collection as problem_collection
import masterthesis.simulation as sim

color_avg_fit_nsc = 'blue'
color_avg_fit_wsc = 'lightblue'
color_avg_dst_nsc = 'orange'
color_avg_dst_wsc = 'red'

legend_loc = 2

policy_colors = ["red", "orange", "yellow", "green", "lightgreen" ,"lightblue", "blue", "purple"]

is_saving_active = True


# credit: https://stackoverflow.com/questions/27214537/is-it-possible-to-draw-a-matplotlib-boxplot-given-the-percentile-values-instead/60415825#60415825
def create_boxplot(tuple):
    name, data = tuple
    box = {
        'label': name,
        'whislo': data[0],  # Bottom whisker position
        'q1': data[1],  # First quartile (25th percentile)
        'med': data[2],  # Median         (50th percentile)
        'q3': data[3],  # Third quartile (75th percentile)
        'whishi': data[4],  # Top whisker position
        'fliers': []  # Outliers
    }
    return box


def get_boxplot_data(df):

    fit_mins = df[pd_settings.dict_fit_min].to_list()
    fit_iqr25 = df[pd_settings.dict_iqr25_fit].to_list()
    fit_medians = df[pd_settings.dict_median_fit].to_list()
    fit_iqr75 = df[pd_settings.dict_iqr75_fit].to_list()
    fit_maxs = df[pd_settings.dict_fit_max].to_list()

    fit_tuple = [(mi, i25, me, i75, mx) for mi, i25, me, i75, mx in zip(fit_mins, fit_iqr25, fit_medians, fit_iqr75, fit_maxs)]

    dst_mins = df[pd_settings.dict_dist_min].to_list()
    dst_iqr25 = df[pd_settings.dict_iqr25_dist].to_list()
    dst_medians = df[pd_settings.dict_median_dist].to_list()
    dst_iqr75 = df[pd_settings.dict_iqr75_dist].to_list()
    dst_maxs = df[pd_settings.dict_dist_max].to_list()

    dst_tuple = [(mi, i25, me, i75, mx) for mi, i25, me, i75, mx in
                 zip(dst_mins, dst_iqr25, dst_medians, dst_iqr75, dst_maxs)]

    return fit_tuple, dst_tuple


def setup_boxplot(df):
    title, file_path = get_title_and_path(df, "box_plots")
    fig, ax = plt.subplots()
    fig.suptitle(title)
    fig.set_size_inches(11, 8)

    # filtering between scan and no scan
    df_scan = pd_settings.filter_dataframe(df, pd_settings.dict_scan, True)
    df_noscan = pd_settings.filter_dataframe(df, pd_settings.dict_scan, False)

    # getting names for x axis
    name_no_scan = df_noscan[pd_settings.dict_orientation].to_list()
    name = [translate(t) for _, t in enumerate(name_no_scan)]
    # name[0] = "SM"

    fit_tuple, dst_tuple = get_boxplot_data(df_scan)

    boxplots = []
    for n, d in zip(name, dst_tuple):
        boxplots.append(create_boxplot((n, d)))

    # print(boxplots)

    ax.bxp(boxplots, showfliers=False)

    # print(data)
    # show plot
    plt.show()


def translate(txt):
    if "straight" in txt:
        txt = "SM"
    elif "no_update" in txt:
        txt = "NU"
    elif "random" in txt:
        txt = "RND"
    elif "default_pso" in txt:
        txt = "PSO"
    elif "next_best_orientation" in txt:
        txt = "GRDY"
    elif "towards_move_direction" in txt:
        txt = "TMD"
    elif "best_position" in txt:
        txt = "TBP"
    elif "swarm" in txt:
        txt = "TSC"
    return txt


def get_title(prob, model, style):

    if style == "iterations":
        return f"{model.capitalize()}: Average achieved values in {sim.iteration_limit} Iterations for {prob}"
    elif style == "energy":
        return f"{model.capitalize()}: Average achieved values with {sim.start_energy} DU for {prob}"
    elif style == "fitness":
        return f"{model.capitalize()}: Average achieved values with fitness threshold {sim.fitness_threshold} for {prob}"
    else:
        return "There was a mistake"


def get_title_and_path(df, diagram_style="bar_charts"):
    # Basic set up
    prob = df[pd_settings.dict_optim_func].tolist()
    prob = prob[0].capitalize()

    style = df[pd_settings.dict_style].to_list()[0]
    model = df[pd_settings.dict_model].to_list()
    # print(model)
    model = model[2]

    title = get_title(prob, model, style)
    file_path = f'../images/result_charts/{diagram_style}/{model}/{prob}-{style}.png'
    return title, file_path


def create_fitness_diagramm(df):
    # Basic set up
    title, file_path = get_title_and_path(df, "bar_charts")

    fig, axes = plt.subplots(nrows=2, ncols=2, sharey="row")
    fig.set_size_inches(11, 8)
    fig.suptitle(title)

    # filtering between scan and no scan
    df_scan = pd_settings.filter_dataframe(df, pd_settings.dict_scan, True)
    df_noscan = pd_settings.filter_dataframe(df, pd_settings.dict_scan, False)

    # getting names for x axis
    name_no_scan = df_noscan[pd_settings.dict_orientation].to_list()
    name = [translate(t) for _, t in enumerate(name_no_scan)]
    name[0] = "SM"

    # filling bars with data
    avg_fit_no_scan = df_noscan[pd_settings.dict_avg_fit].to_list()
    var_fit_no_scan = df_noscan[pd_settings.dict_var_fit].to_list()
    data_fit_no_scan = [(avg, var, name, col) for avg, var, name, col in zip(avg_fit_no_scan, var_fit_no_scan, name, policy_colors)]

    avg_dist_no_scan = df_noscan[pd_settings.dict_avg_dist].to_list()
    var_dist_no_scan = df_noscan[pd_settings.dict_var_dist].to_list()
    data_dist_no_scan = [(avg, var, name, col) for avg, var, name, col in zip(avg_dist_no_scan, var_dist_no_scan, name, policy_colors)]

    avg_fit_scan = df_scan[pd_settings.dict_avg_fit].to_list()
    var_fit_scan = df_scan[pd_settings.dict_var_fit].to_list()
    data_fit_scan = [(avg, var, name, col) for avg, var, name, col in zip(avg_fit_scan, var_fit_scan, name, policy_colors)]

    avg_dist_scan = df_scan[pd_settings.dict_avg_dist].to_list()
    var_dist_scan = df_scan[pd_settings.dict_var_dist].to_list()
    data_dist_scan = [(avg, var, name, col) for avg, var, name, col in zip(avg_dist_scan, var_dist_scan, name, policy_colors)]

    style_one(axes, data_fit_no_scan, data_dist_no_scan, data_fit_scan, data_dist_scan)
    # style_two(axes, name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)

    if is_saving_active:
        plt.show(block=False)
        fig.savefig(file_path, dpi=fig.dpi)
        plt.close(fig)
    else:
        plt.show()
        plt.close(fig)


def style_one(axes, dfns, ddns, dfs, dds):
    '''
    Create a 2x2 subplot figure where each subplot represents a category like fitness or distance
    :param axes: all subplots
    :param dfns: data_fitness_no_scan
    :param ddns: data_distance_no_scan
    :param dfs: data_fitness_scan
    :param dds: data_distance_scan
    :return:
    '''
    # sort?
    dfns.sort()
    ddns.sort()
    dfs.sort()
    dds.sort()

    max_fit = dfns[-1][0] if dfns[-1][0] > dfs[-1][0] else dfs[-1][0]
    max_fit += (max_fit / 10)
    max_dst = ddns[-1][0] if ddns[-1][0] > dds[-1][0] else dds[-1][0]
    max_dst += (max_dst / 10)

    create_subplot(axes[0, 0], dfns, "Fitness", "No Scan", color_avg_fit_nsc, max_fit)
    create_subplot(axes[0, 1], dfs, "Fitness", "With Scan", color_avg_fit_wsc, max_fit)
    create_subplot(axes[1, 0], ddns, "Distance", "", color_avg_dst_nsc, max_dst)
    create_subplot(axes[1, 1], dds, "Distance", "", color_avg_dst_wsc, max_dst)


def style_two(axes, name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan):
    '''
    Show a 2x2 subplot figure where each subplots shows all categories but each plot sorted based on one category
    :param axes:
    :param name:
    :param avg_fit_no_scan:
    :param avg_fit_scan:
    :param avg_dist_no_scan:
    :param avg_dist_scan:
    :return:
    '''
    l1, l2, l3, l4 = sort_list_by_group(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)

    create_sub_plot_style2(axes[0, 0], l1)
    create_sub_plot_style2(axes[0, 1], l2)
    create_sub_plot_style2(axes[1, 0], l3)
    create_sub_plot_style2(axes[1, 1], l4)


def sort_list_by_group(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan):
    l1 = [(fns, dns, fws, dws, n) for n, fns, fws, dns, dws in
          zip(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)]

    l2 = [(fns, dns, fws, dws, n) for n, fns, fws, dns, dws in
          zip(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)]

    l3 = [(fns, dns, fws, dws, n) for n, fns, fws, dns, dws in
          zip(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)]

    l4 = [(fns, dns, fws, dws, n) for n, fns, fws, dns, dws in
          zip(name, avg_fit_no_scan, avg_fit_scan, avg_dist_no_scan, avg_dist_scan)]

    l1.sort(key=lambda t: t[0])
    l2.sort(key=lambda t: t[1])
    l3.sort(key=lambda t: t[2])
    l4.sort(key=lambda t: t[3])
    return l1, l2, l3, l4


def create_all_boxplots(df, experiment_style):

    for problem_key in problem_collection.get_problems().keys():
        problem_df = pd_settings.filter_dataframe(df, pd_settings.dict_optim_func, problem_key)
        for model in ["dubins", "reed_shepp", "bezier"]:
            model_df = pd_settings.filter_dataframe(problem_df, pd_settings.dict_model, model)
            for style in experiment_style:
                filtered_df = pd_settings.filter_dataframe(model_df, pd_settings.dict_style, style)
                # print(filtered_df)
                if not filtered_df.empty:
                    setup_boxplot(filtered_df)


def create_all_diagrammes(df):
    for problem_key in problem_collection.get_problems().keys():
        problem_df = pd_settings.filter_dataframe(df, pd_settings.dict_optim_func, problem_key)
        for model in ["dubins", "reed_shepp", "bezier"]:
            model_df = pd_settings.filter_dataframe(problem_df, pd_settings.dict_model, model)
            for style in ["fitness", "energy", "iterations"]:
                filtered_df = pd_settings.filter_dataframe(model_df, pd_settings.dict_style, style)
                if len(filtered_df.index) > 2:
                    create_fitness_diagramm(filtered_df)



def create_subplot(sub_ax, avg_var_name_color_tuple, y_axis_name, title, color, max_y_value):
    avg = [avg for (avg, _, _, _) in avg_var_name_color_tuple]
    stdup = [np.sqrt(var) for (_, var, _, _) in avg_var_name_color_tuple]
    stddown = [u if u < a else a for u, a in zip(stdup, avg)]
    name = [name for (_, _, name, _) in avg_var_name_color_tuple]
    pol_colors = [col for (_, _, _, col) in avg_var_name_color_tuple]

    idx = np.arange(len(name))

    sub_ax.set_ylabel(y_axis_name)
    bars = sub_ax.bar(idx, avg, edgecolor="black", color=pol_colors, label="T")  #, yerr=(stddown, stdup))

#    for index in range(len(avg)):
#        bars[index].set_color(policy_colors[index])

    y_lim1 = sub_ax.get_ylim()
    sub_ax.set_ylim(0, max_y_value)

    sub_ax.title.set_text(title)

    plt.sca(sub_ax)
    plt.xticks(idx, name)
    # bar1 = ax.bar(idx - width / 2 - width / 4, avg_fit_no_scan, width=width / 2, edgecolor="black",
    #              color=color_avg_fit_nsc, label="Fitness wo. Scan")  # , yerr=var_fit_no_scan, )


def create_sub_plot_style2(sub_ax, tuple_list):
    avg_fit_nsc = [fv for (fv, _, _, _, _) in tuple_list]
    avg_dst_nsc = [sv for (_, sv, _, _, _) in tuple_list]
    avg_fit_wsc = [tv for (_, _, tv, _, _) in tuple_list]
    avg_dst_wsc = [fo for (_, _, _, fo, _) in tuple_list]
    name = [n for (_, _, _, _, n) in tuple_list]

    width = 0.35

    idx = np.arange(len(name))

    bar1 = sub_ax.bar(idx-width/2-width/4, avg_fit_nsc, width=width/2, edgecolor="black", color=color_avg_fit_nsc, label="Fitness wo. Scan")#, yerr=var_fit_no_scan, )
    bar2 = sub_ax.bar(idx + width/2-width/4, avg_fit_wsc, width=width/2, edgecolor="black", color=color_avg_fit_wsc, label="Fitness w/ Scan")#, yerr=var_fit_scan)
    plt.xlabel("Policy")
    sub_ax.set_ylabel("Fitness")
    y_lim1 = sub_ax.get_ylim()
    sub_ax.set_ylim(0, y_lim1[1] + y_lim1[1] / 5)

    ax2 = sub_ax.twinx()
    bar3 = ax2.bar(idx-width/2+width/4, avg_dst_nsc, width=width/2, edgecolor="black", color=color_avg_dst_nsc, label="Distance wo. Scan")#, yerr=var_dist_no_scan)
    bar4 = ax2.bar(idx + width / 2 + width / 4, avg_dst_wsc, width=width/2, edgecolor="black", color=color_avg_dst_wsc, label="Distance w. Scan")#yerr=var_dist_scan, )

    y_lim2 = ax2.get_ylim()
    ax2.set_ylim(0, y_lim2[1] + y_lim2[1] / 5)
    ax2.set_ylabel("Distance")
    bars = [bar1, bar2, bar3, bar4]
    labels = [b.get_label() for b in bars]

    # sub_ax.legend(bars, labels, loc=legend_loc)
    # ax2.legend(bars, labels, loc=legend_loc)

    plt.xticks(idx, name)


is_using_boxplot = True

if __name__ == "__main__":
    path = "../results/1609774586915_all.csv"
    dataframe = pd.read_csv(path)
    if is_using_boxplot:
        create_all_boxplots(dataframe, ["iterations"])
    else:
        create_all_diagrammes(dataframe)
        print("done")
