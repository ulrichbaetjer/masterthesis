import math
from abc import ABC

from masterthesis.optimization_function.optimization_base_class import OptimizationBase


class AckleyOptimization(OptimizationBase, ABC):

    @staticmethod
    def get_optimum_position():
        return [0, 0]

    @staticmethod
    def get_max_values():
        return [5, 5]

    @staticmethod
    def get_min_values():
        return [-5, -5]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def evaluate(pos):
        return -20 * math.e ** (-0.2 * math.sqrt(0.5 * (pos[0]**2 + pos[1]**2))) -\
               math.e ** (0.5 * (math.cos(2 * math.pi * pos[0]) + math.cos(2 * math.pi * pos[1]))) +\
               math.e + 20

    @staticmethod
    def get_name():
        return "Ackley"

