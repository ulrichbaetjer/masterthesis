'''
For the case that I want to replace my benchmark problems with the deap ones
'''

from masterthesis.optimization_function.problem_collection import *
from deap import benchmarks as deap_benchmarks
import numpy as np


def sphere(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.sphere(p)[0] for p in point])
    return deap_benchmarks.sphere(point)[0]


def plane(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.plane(p)[0] for p in point])
    return deap_benchmarks.plane(point)[0]


def rastrigin(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.rastrigin(p)[0] for p in point])
    return deap_benchmarks.rastrigin(point)[0]


def ackley(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.ackley(p)[0] for p in point])
    return deap_benchmarks.ackley(point)[0]


def rosenbrock(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.rosenbrock(p)[0] for p in point])
    return deap_benchmarks.rosenbrock(point)[0]


def griewank(point):
    if point.ndim == 2:
        return np.array([deap_benchmarks.griewank(p)[0] for p in point])
    return deap_benchmarks.griewank(point)[0]


if __name__ == "__main__":
    points = np.array([[1, 1], [0, 0], [2, 2], [3, 3], [4, 16]])
    for a in points:
        print((rosenbrock(a), RosenbrockOptimization.evaluate(a)), " | ", (ackley(a), AckleyOptimization.evaluate(a)))
