class OptimizationBase:
    x = 0
    y = 1

    @staticmethod
    def get_name():
        raise NotImplementedError(f"hasn't implemented the get_name() function yet")

    @staticmethod
    def get_optimum_position():
        raise NotImplementedError(f"hasn't implemented the get_optimum_position() function yet")

    @staticmethod
    def get_max_values():
        raise NotImplementedError(f"hasn't implemented the get_max_values() function yet")

    @staticmethod
    def get_min_values():
        raise NotImplementedError(f"hasn't implemented the get_min_values() function yet")

    @staticmethod
    def get_problem_dimension_size():
        raise NotImplementedError(f"hasn't implemented the get_problem_dimension_size() function yet")

    @staticmethod
    def evaluate(pos):
        raise NotImplementedError(f"hasn't implemented the evaluate() function yet")

    @staticmethod
    def optimal_fitness_value():
        return 0.0
