from masterthesis.optimization_function.optimization_base_class import OptimizationBase
import math


def _eval_help(dimensional_position):
    return dimensional_position ** 2 - 10 * math.cos(2 * math.pi * dimensional_position)


class RastriginOptimization(OptimizationBase):
    @staticmethod
    def get_name():
        return "Rastrigin"

    @staticmethod
    def get_optimum_position():
        return [0, 0]

    @staticmethod
    def get_max_values():
        return [5.12, 5.12]

    @staticmethod
    def get_min_values():
        return [-5.12, -5.12]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def evaluate(pos):
        return 20 + _eval_help(pos[0]) + _eval_help(pos[1])


