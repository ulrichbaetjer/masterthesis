from masterthesis.optimization_function.optimization_base_class import OptimizationBase


class BealeOptimization(OptimizationBase):
    @staticmethod
    def get_name():
        return "Beale"

    @staticmethod
    def get_optimum_position():
        return [3, 0.5]

    @staticmethod
    def get_max_values():
        return [4.5, 4.5]

    @staticmethod
    def get_min_values():
        return [-4.5, -4.5]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def __part_one(x, y):
        return (1.5 - x + x * y) ** 2

    @staticmethod
    def __part_two(x, y):
        return (2.25 - x + (x * y ** 2)) ** 2

    @staticmethod
    def __part_three(x, y):
        return (2.625 - x + (x * y ** 3)) ** 2


    @staticmethod
    def evaluate(pos):
        return BealeOptimization.__part_one(pos[0], pos[1]) \
               + BealeOptimization.__part_two(pos[0], pos[1]) \
               + BealeOptimization.__part_three(pos[0], pos[1])
