from masterthesis.optimization_function.rosenbrock_func import RosenbrockOptimization
from masterthesis.optimization_function.sphere_func import SphereOptimization
from masterthesis.optimization_function.beale_func import BealeOptimization
from masterthesis.optimization_function.ackley_func import AckleyOptimization
from masterthesis.optimization_function.bukin_six_func import BukinSixOptimization
from masterthesis.optimization_function.rastrigin_func import RastriginOptimization


def get_problems() -> dict:
    dictionary = {
        RosenbrockOptimization.get_name(): RosenbrockOptimization,
        SphereOptimization.get_name(): SphereOptimization,
        BealeOptimization.get_name(): BealeOptimization,
        AckleyOptimization.get_name(): AckleyOptimization,
        BukinSixOptimization.get_name(): BukinSixOptimization,
        RastriginOptimization.get_name(): RastriginOptimization
    }
    return dictionary
