from masterthesis.optimization_function.optimization_base_class import OptimizationBase


class RosenbrockOptimization(OptimizationBase):
    @staticmethod
    def get_name():
        return "Rosenbrock"

    @staticmethod
    def get_optimum_position():
        return [1, 1]

    @staticmethod
    def get_max_values():
        return [5, 5]

    @staticmethod
    def get_min_values():
        return [-5, -5]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def evaluate(pos):
        return 100 * (pos[1] - pos[0] ** 2)**2 + (1 - pos[0]) ** 2
