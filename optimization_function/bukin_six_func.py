from masterthesis.optimization_function.optimization_base_class import OptimizationBase
import math


class BukinSixOptimization(OptimizationBase):
    @staticmethod
    def get_name():
        return "Bukin N.6"

    @staticmethod
    def get_optimum_position():
        return [-10, 1]

    @staticmethod
    def get_max_values():
        return [-5, 3]

    @staticmethod
    def get_min_values():
        return [-15, -3]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def evaluate(pos):
        return 100 * math.sqrt(abs(pos[1] - 0.01 * pos[0]**2)) + 0.01 * abs(pos[0] + 10)
