from masterthesis.optimization_function.optimization_base_class import OptimizationBase


class SphereOptimization(OptimizationBase):
    @staticmethod
    def get_name():
        return "Sphere"

    @staticmethod
    def get_optimum_position():
        return [0, 0]

    @staticmethod
    def get_max_values():
        return [50, 50]

    @staticmethod
    def get_min_values():
        return [-50, -50]

    @staticmethod
    def get_problem_dimension_size():
        return 2

    @staticmethod
    def evaluate(pos):
        return pos[0]**2 + pos[1]**2
