import masterthesis.helper_functions.list_functions as list_funcs
from random import Random


class Particle:

    def __init__(self, pos=None, inertia=1.0, s_weight=1.0, c_weight=1.0, rand=Random()):

        self.previous_velocity = None
        self.previous_position = None   # for a cleaner animation
        self.previous_best_pos = pos
        self.position = pos
        self.size_dimension = len(pos)
        self.personal_best = pos
        self.global_best = [0] * self.size_dimension

        # momentum
        self.velocity = [0] * self.size_dimension
        self.inertia = inertia

        # social components
        self.turbulence_global = [0] * self.size_dimension
        self.social_weight = s_weight

        # cognitive components
        self.randomness = rand
        self.turbulence_local = [0] * self.size_dimension
        self.cognitive_weight = c_weight

        # distance travelled
        self.total_distance_travelled = 0
        self.last_distance = 0

    # Update personal or global best positions after calculating the new positions
    def update_global_best(self, new_global_best):
        self.global_best = new_global_best

    def update_personal_best(self, new_personal_best):
        self.previous_best_pos = self.personal_best
        self.personal_best = new_personal_best

    def update_position(self):
        self.previous_velocity = self.velocity
        self.update_velocity()

        self.previous_position = self.position
        new_position = self.calculate_new_position()

        if len(new_position) != self.size_dimension:
            raise AssertionError("The size of dimensions changed", len(new_position), self.size_dimension)

        distance = list_funcs.euclidean_distance(new_position, self.position)

        self.update_distance_travelled(distance)
        self.position = new_position

    def update_velocity(self):
        self.turbulence_local = [self.randomness.uniform(0.0, 1.0) for _ in range(len(self.turbulence_local))]
        self.turbulence_global = [self.randomness.uniform(0.0, 1.0) for _ in range(len(self.turbulence_global))]

        for index, old_velocity in enumerate(self.velocity):
            self.velocity[index] = self.calculate_new_velocity(old_velocity) +\
                                   self.calculate_social_value(index) +\
                                   self.calculate_memory_value(index)

    # PSO calculates a new position based on momentum, social and memory components
    # these are just helper functions to create a less verbose update function
    def calculate_new_velocity(self, vel_in_dimension):
        return self.inertia * vel_in_dimension

    def calculate_social_value(self, index):
        return self.social_weight * self.turbulence_global[index] * (self.global_best[index] - self.position[index])

    def calculate_memory_value(self, index):
        return self.cognitive_weight * self.turbulence_local[index] * (self.personal_best[index] - self.position[index])

    def update_distance_travelled(self, distance):
        self.total_distance_travelled += distance
        self.last_distance = distance

    def calculate_new_position(self):
        new_position = []
        for pos_in_dim, velocity_in_dim in zip(self.position, self.velocity):
            new_pos_in_dim = pos_in_dim + velocity_in_dim
            new_position.append(new_pos_in_dim)

        return new_position

    # compatibility with the inheriting particle classes
    def move_particle(self):
        pass

