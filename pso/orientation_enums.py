from enum import Enum


class OrientationEnums(Enum):
    no_update = 0
    random = 1
    default_pso = 2
    greedy = 3
    straight_model_policy = 4
    towards_best_position = 5
    towards_swarm_center = 6
