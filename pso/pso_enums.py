from enum import Enum


class PsoEnums(Enum):
    straight = 1
    dubins = 2
    reeds_shepp = 3
    bezier = 4
    rtr = 5
