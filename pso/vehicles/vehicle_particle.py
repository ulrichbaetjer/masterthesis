from masterthesis.pso.particle import Particle
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.pso.orientation_enums import OrientationEnums
import masterthesis.helper_functions.list_functions as list_funcs
import random
import math


class VehicleParticle(Particle):

    def __init__(self, pos=None, inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):
        super().__init__(pos, inertia, s_weight, c_weight, rand)
        self.step_size = vehicle_description.step_size
        self.is_normalized_sample_rate = vehicle_description.normalize_sampling
        self.turn_radius = vehicle_description.turn_radius
        self.orientation_style = vehicle_description.orientation_style
        self.remaining_energy = vehicle_description.remaining_energy
        self.measure_while_driving = vehicle_description.measure_while_driving
        self.could_not_move = False
        self.path = None
        self.orientation_rng = rand_orientation

    def update_position(self):
        self.previous_velocity = self.velocity
        self.update_velocity()

        new_position = self.calculate_new_position()

        # work on orientation here
        self.update_orientation(new_position)

        if len(new_position) != self.size_dimension:
            raise AssertionError("The size of dimensions changed", len(new_position), self.size_dimension)

        self.previous_position = self.position
        self.position = new_position

    def update_orientation(self, position):
        if self.orientation_style == OrientationEnums.no_update:
            position[-1] = self.position[-1]  # Use the old position

        elif self.orientation_style == OrientationEnums.random:
            position[-1] = self.orientation_rng.uniform(0, math.tau)

        elif self.orientation_style == OrientationEnums.greedy:
            position[-1] = self.find_next_best_orientation(self.position, position)

        elif self.orientation_style == OrientationEnums.straight_model_policy:
            angle = list_funcs.calculate_angle(self.position[:-1], position[:-1])
            position[-1] = angle if not math.isnan(angle) else self.position[-1]

        elif self.orientation_style == OrientationEnums.towards_best_position:
            position[-1] = math.nan  # should throw an exception if left untouched

        elif self.orientation_style == OrientationEnums.towards_swarm_center:
            position[-1] = math.nan  # should throw an exception if left untouched

        else:
            pass

    def move_particle(self):

        # euclidean_distance = list_funcs.euclidean_distance(self.previous_position[:-1], self.position[:-1])
        # for now, ignore this and see how it turns out
        # euclidean_distance > 1e-15 and

        path = self.create_path(self.previous_position, self.position, self.turn_radius)
        path_len = self.get_path_length(path)

        if path_len < self.remaining_energy:
            self.could_not_move = False
            self.remaining_energy -= path_len
            self.update_distance_travelled(path_len)

        else:
            self.could_not_move = True
            if self.position == self.personal_best:
                self.personal_best = self.previous_best_pos

            self.position = self.previous_position
            self.velocity = self.previous_velocity
            path = self.create_path(self.position, self.position, self.turn_radius)
            self.update_distance_travelled(0)

        self.path = path

    def create_path(self, start, end, turn):
        raise NotImplementedError("VehicleParticle is an abstract class and can't create its own path")

    def get_path_length(self, path):
        raise NotImplementedError("VehicleParticle is an abstract class and can't calculate its path length")

    def get_path_sample(self, path):
        raise NotImplementedError("VehicleParticle is an abstract class and can't sample its path")

    def find_next_best_orientation(self, s, g):
        parts = 40  # basically check out every 9° step
        fraction = math.tau / parts
        start_angle = s[-1]
        l = []
        for denom in range(parts):
            frac = (denom * fraction + start_angle) % math.tau
            t = (g[0], g[1], frac)
            p = self.create_path(s, t, self.turn_radius)
            l.append((self.get_path_length(p), frac))

        l.sort(key=lambda tup: tup[0])
        return l[0][1]

    def normalize_sample_rate(self):
        if self.is_normalized_sample_rate:
            self.step_size = self.last_distance / 5
            self.step_size = 0.000001 if self.step_size < 0.000001 else self.step_size

