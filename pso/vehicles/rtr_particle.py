import random
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.pso.vehicles.vehicle_particle import VehicleParticle
from masterthesis.helper_functions.rtr_model import Rtr


class RTRParticle(VehicleParticle):
    def __init__(self, pos=None, inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):
        super().__init__(pos, inertia, s_weight, c_weight, vehicle_description, rand, rand_orientation)

    def create_path(self, start, end, turn):
        self.normalize_sample_rate()
        return Rtr(start, end, self.step_size)

    def get_path_length(self, path):
        # as the RTR model shares the same path length as the straight model,
        # it is more interesting to see how much it needs to rotate
        return path.get_rotation_change()

    def get_path_sample(self, path):
        return path.path
