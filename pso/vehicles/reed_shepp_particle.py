# https://github.com/ghliu/pyReedsShepp

from masterthesis.pso.vehicles.vehicle_particle import VehicleParticle
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
import random
import reeds_shepp


class ReedsSheppParticle(VehicleParticle):
    def __init__(self, pos=None,  inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):            # vehicle components
        super().__init__(pos, inertia, s_weight, c_weight, vehicle_description, rand, rand_orientation)

    def create_path(self, start, end, turn):
        return reeds_shepp.PyReedsSheppPath(start, end, turn)

    def get_path_length(self, path):
        return path.distance()

    def get_path_sample(self, path):
        self.normalize_sample_rate()
        return path.sample(self.step_size)
