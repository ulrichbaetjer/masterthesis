from masterthesis.pso.vehicles.vehicle_particle import VehicleParticle
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
import random
import dubins
import math


class DubinsParticle(VehicleParticle):

    def __init__(self, pos=None, dim_size=3, inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):            # vehicle components
        super().__init__(pos, inertia, s_weight, c_weight, vehicle_description, rand, rand_orientation)

    def create_path(self, start, end, turn):
        return dubins.shortest_path(start, end, turn)

    def get_path_length(self, path):
        return path.path_length()

    def get_path_sample(self, path):
        self.normalize_sample_rate()
        samples, _ = path.sample_many(self.step_size)
        return samples

    # possible alternative to the greedy brute force
    def __greedy_binary_search(self, x, y):
        a = self.position[-1]
        b = self.position[-1] + math.pi
        rad_distance = (a + b) / 2
        best_rad = None

        path_a = dubins.shortest_path(self.position, (x, y, a), self.turn_radius)
        path_b = dubins.shortest_path(self.position, (x, y, b), self.turn_radius)
        len_a = path_a.path_length()
        len_b = path_b.path_length()

        for _ in range(4):

            if len_a < len_b:
                b1 = b + rad_distance
                b2 = b - rad_distance
                len_b1 = dubins.shortest_path(self.position, (x, y, b1), self.turn_radius).path_length()
                len_b2 = dubins.shortest_path(self.position, (x, y, b2), self.turn_radius).path_length()
                if len_b1 < len_b2:
                    len_b = len_b1
                    b = b1
                else:
                    len_b = len_b2
                    b = b2
            else:
                a1 = a + rad_distance
                a2 = a - rad_distance
                len_a1 = dubins.shortest_path(self.position, (x, y, a1), self.turn_radius).path_length()
                len_a2 = dubins.shortest_path(self.position, (x, y, a2), self.turn_radius).path_length()
                if len_a1 < len_a2:
                    len_a = len_a1
                    a = a1
                else:
                    len_a = len_a2
                    a = a2

            if a < 0:
                a += math.tau
            elif a > math.tau:
                a -= math.tau
            if b < 0:
                b += math.tau
            elif b > math.tau:
                b -= math.tau
            best_rad = a if len_a < len_b else b
            rad_distance /= 2

        return best_rad
