from masterthesis.pso.orientation_enums import OrientationEnums
import math


'''
This class is a simple description for the vehicle model.
It's main purpose is to have a cleaner constructor for the Particle class


:param orientation_style: Describes how the pso algorithm deals with the vehicle orientation
:param turn_radius: Describes the radius the vehicle can turn to reach its destination
:param step_size: Describes how many samples the path returns during its movement from A to B
'''


class VehicleDescription:

    def __init__(self, orientation_style=OrientationEnums.no_update, turn_radius=1.0, step_size=0.2,
                 fuel=math.inf, scanner=False, is_normalized=False):
        self.orientation_style = orientation_style
        self.turn_radius = turn_radius
        self.step_size = step_size
        self.remaining_energy = fuel
        self.measure_while_driving = scanner
        self.normalize_sampling = is_normalized

