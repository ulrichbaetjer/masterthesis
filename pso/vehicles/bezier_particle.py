import bezier

import random
from masterthesis.pso.vehicles.vehicle_particle import VehicleParticle
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription

import masterthesis.helper_functions.bezier_functions as bez_func


class BezierParticle(VehicleParticle):
    def __init__(self, pos=None, inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):            # vehicle components
        super().__init__(pos, inertia, s_weight, c_weight, vehicle_description, rand, rand_orientation)

    def create_path(self, start, end, turn):
        nodes = bez_func.calculate_control_points(start, end, turn)
        return bezier.curve.Curve(nodes, degree=3)

    def get_path_length(self, path):
        return path.length

    def get_path_sample(self, path):
        self.normalize_sample_rate()
        return bez_func.calculate_samples(path, self.previous_position, self.position, self.step_size)
