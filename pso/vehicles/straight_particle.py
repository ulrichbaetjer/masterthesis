from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.vehicle_particle import VehicleParticle
import reeds_shepp
import math
import random
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
import masterthesis.helper_functions.list_functions as list_func


class StraightParticle(VehicleParticle):
    def __init__(self, pos=None, inertia=1.0,
                 s_weight=1.0,  # social components
                 c_weight=1.0,  # memory components
                 vehicle_description=VehicleDescription(),
                 rand=random.Random(), rand_orientation=random.Random()):
        vehicle_description.orientation_style = OrientationEnums.straight_model_policy
        super().__init__(pos, inertia, s_weight, c_weight, vehicle_description, rand, rand_orientation)

    def update_orientation(self, new_position):
        angle = list_func.calculate_angle(self.position[:-1], new_position[:-1])

        if not math.isnan(angle):
            self.position[-1] = angle
            new_position[-1] = angle

        else:
            new_position[-1] = self.position[-1]

    def create_path(self, start, end, turn):
        return reeds_shepp.PyReedsSheppPath(start, end, turn)

    def get_path_length(self, path):
        return path.distance()

    def get_path_sample(self, path):
        self.normalize_sample_rate()
        return path.sample(self.step_size)
