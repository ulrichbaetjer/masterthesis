import math
from random import Random

from masterthesis.pso.particle import Particle
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from masterthesis.pso.vehicles.straight_particle import StraightParticle
from masterthesis.pso.vehicles.reed_shepp_particle import ReedsSheppParticle
from masterthesis.pso.vehicles.bezier_particle import BezierParticle
from masterthesis.pso.vehicles.rtr_particle import RTRParticle

from masterthesis.pso.pso_enums import PsoEnums
from masterthesis.pso.orientation_enums import OrientationEnums

from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.optimization_function.ackley_func import AckleyOptimization
from masterthesis.visualization.particle_path import ParticlePath
from masterthesis.helper_functions.list_functions import calculate_angle

dict_vehicle_class = {PsoEnums.dubins: DubinsParticle,
                      PsoEnums.reeds_shepp: ReedsSheppParticle,
                      PsoEnums.bezier: BezierParticle,
                      PsoEnums.rtr: RTRParticle,
                      PsoEnums.straight: StraightParticle}


class Swarm:
    def __init__(self, particles=None, size=1, optimization_function=AckleyOptimization, swarm_type=None,
                 vehicle_description=None, randomness=Random(), rand_orientation=Random()):
        if size < 1 and particles is None:
            raise ValueError("The swarm must consist of 1 or more particles")

        self.current_generation = 0
        self.sum_max_dist_per_gen = 0
        self.opt_func = optimization_function
        self.swarm_type = swarm_type
        self.randomness = randomness
        self.orientation_randomness = rand_orientation

        if particles is None:
            self.size = size
            self.particles = [None] * size
            self.initialize_swarm(vehicle_description)

        else:
            self.particles = particles
            self.size = len(particles)

        self.best_found_position = None
        self.best_fitness_value = None
        self.update_global_best()

    def initialize_swarm(self, vehicle_description):
        problem_dimension_size = self.opt_func.get_problem_dimension_size()
        for index in range(self.size):

            pos = self.create_spawn_position()

            if self.swarm_type is None:
                self.particles[index] = Particle(pos=pos, inertia=0.5,
                                                 rand=self.randomness)

            else:
                # give vehicle a random start orientation
                pos.append(self.randomness.uniform(0, math.tau))

                if vehicle_description is None:
                    vehicle_description = VehicleDescription()

                particle_type = dict_vehicle_class[self.swarm_type]

                self.particles[index] = particle_type(pos,
                                                      inertia=0.5,
                                                      vehicle_description=vehicle_description,
                                                      c_weight=1.4, s_weight=1.4,
                                                      rand=self.randomness,
                                                      rand_orientation=self.orientation_randomness)

    def get_current_best_position_old(self):
        best_pos = None
        best_score = math.inf
        for particle in self.particles:
            score = self.opt_func.evaluate(particle.position)
            if score < best_score:
                best_pos = particle.position
                best_score = score

        return best_pos

    def get_current_best_position(self):
        best_pos = None
        best_score = math.inf
        for particle in self.particles:
            score = self.opt_func.evaluate(particle.personal_best)
            if score < best_score:
                best_pos = particle.personal_best
                best_score = score

        return best_pos

    def calculate_angles_towards_swarm_center(self):
        center_of_swarm = self.calculate_center_of_swarm()

        for particle in self.particles:
            if not type(particle) == Particle and particle.orientation_style == OrientationEnums.towards_swarm_center:
                angle = calculate_angle(particle.position[:-1], center_of_swarm)

                particle.position[-1] = angle if not math.isnan(angle) else particle.previous_position[-1]

    def calculate_angles_towards_best_particle(self):

        for particle in self.particles:
            if not type(particle) == Particle and particle.orientation_style == OrientationEnums.towards_best_position:
                angle = calculate_angle(particle.position[:-1], self.best_found_position)

                particle.position[-1] = angle if not math.isnan(angle) else \
                    particle.find_next_best_orientation(particle.previous_position, particle.position)

    def update_swarm(self):
        for particle in self.particles:
            particle.update_position()

        self.calculate_angles_towards_swarm_center()
        self.calculate_angles_towards_best_particle()

        max_travelled = 0
        for particle in self.particles:

            particle.move_particle()
            if self.opt_func.evaluate(particle.position) < self.opt_func.evaluate(particle.personal_best):
                self.update_personal_best(particle)

            if max_travelled < particle.last_distance:
                max_travelled = particle.last_distance

        self.update_global_best()

        self.sum_max_dist_per_gen += max_travelled
        self.current_generation += 1

    def update_global_best(self):
        best_position = self.get_current_best_position()
        if self.best_found_position != best_position:
            self.best_found_position = best_position
            self.best_fitness_value = self.opt_func.evaluate(self.best_found_position)
        for particle in self.particles:
            particle.update_global_best(best_position)

    def update_personal_best(self, particle):
        if type(particle) == Particle or not particle.measure_while_driving:
            particle.update_personal_best(particle.position)
        else:
            sample = particle.get_path_sample(particle.path)

            for s in sample:
                if self.opt_func.evaluate(s) < self.opt_func.evaluate(particle.personal_best):
                    particle.update_personal_best(s)
            if len(sample) == 0:
                particle.update_personal_best(particle.position)

    def create_spawn_position(self):
        problem_dimension_size = self.opt_func.get_problem_dimension_size()
        pos = [0] * problem_dimension_size
        min_values = self.opt_func.get_min_values()
        max_values = self.opt_func.get_max_values()
        for index in range(problem_dimension_size):
            pos[index] = self.randomness.uniform(min_values[index], max_values[index])

        return pos

    def calculate_center_of_swarm(self):
        avg_xpos = 0
        avg_ypos = 0
        for particle in self.particles:
            avg_xpos += particle.position[0]
            avg_ypos += particle.position[1]
        return [avg_xpos / self.size, avg_ypos / self.size]

    def get_swarm_positions(self):
        xpos = []
        ypos = []

        for p in self.particles:
            xpos.append(p.position[0])
            ypos.append(p.position[1])

        return xpos, ypos

    def get_average_path_length(self):
        avg = 0
        for p in self.particles:
            avg += p.last_distance
        avg /= self.size
        return avg

    def get_longest_path_length(self):
        max_length = 0
        for p in self.particles:
            if p.last_distance > max_length:
                max_length = p.last_distance
        return max_length

    def get_average_distance_travelled(self):
        avg = 0
        for p in self.particles:
            avg += p.total_distance_travelled
        avg /= self.size
        return avg

    def get_average_fitness(self):
        sum_fitness = 0
        for p in self.particles:
            sum_fitness += self.opt_func.evaluate(p.position)
        return sum_fitness / self.size

    def get_swarm_movement(self):
        p_moves = []
        for p in self.particles:
            p_moves.append(ParticlePath(p))
        return p_moves

    def has_swarm_moved(self):
        has_moved = False
        if self.swarm_type == PsoEnums.default:
            has_moved = True
        else:
            for p in self.particles:
                if not p.could_not_move:
                    has_moved = True
                    break
        return has_moved

    def calculate_nmec(self):
        distance = 0
        for p in self.particles:
            distance += p.total_distance_travelled
        return distance / (self.current_generation * self.size)

    def calculate_nmtc(self):
        return self.sum_max_dist_per_gen / self.current_generation
