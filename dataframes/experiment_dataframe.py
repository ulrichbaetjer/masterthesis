import pandas as pd
import masterthesis.dataframes.pandas_settings as pd_option
from masterthesis.pso.swarm import Swarm

exp_df = None
row_list = []


def get_columns():
    return [
        pd_option.dict_model,
        pd_option.dict_orientation,
        pd_option.dict_scan,
        pd_option.dict_nsr,
        pd_option.dict_size,
        pd_option.dict_optim_func,
        pd_option.dict_run_seed,
        pd_option.dict_run_number,
        pd_option.dict_generation,
        pd_option.dict_fit,
        pd_option.dict_avg_dist,
        pd_option.dict_avg_path_len,
        pd_option.dict_longest_path_len
        ]


def get_data_frame():
    global exp_df
    if exp_df is None:
        pd_option.setup_dataframe()

    exp_df = pd.DataFrame(columns=get_columns())
    exp_df = exp_df.append(row_list, ignore_index=True)

    return exp_df


def update_data_frame(values: list):

    if len(values) != len(get_columns()):
        raise AssertionError(f"DataFrame doesn't exist or number of values ({len(values)} " +
                             f"don't match columns ({len(get_columns())})")

    global row_list
    row_list.append(dict(zip(get_columns(), values)))


def extract_swarm_data(swarm: Swarm, run_id, run_seed):

    values = [swarm.swarm_type.name,
              swarm.particles[0].orientation_style,
              swarm.particles[0].measure_while_driving,
              swarm.particles[0].is_normalized_sample_rate,
              swarm.size,
              swarm.opt_func.get_name(),
              run_seed,
              run_id,
              swarm.current_generation,
              swarm.best_fitness_value,
              swarm.get_average_distance_travelled(),
              swarm.get_average_path_length(),
              swarm.get_longest_path_length()
              ]

    return values


def reset_data_frame():
    global exp_df
    exp_df = None
    global row_list
    row_list = []

