import pandas as pd
import masterthesis.dataframes.pandas_settings as pd_option


sum_df = None


def get_columns():
    return [pd_option.dict_model,
            pd_option.dict_orientation,
            pd_option.dict_style,
            pd_option.dict_scan,
            pd_option.dict_nsr,
            pd_option.dict_size,
            pd_option.dict_optim_func,
            pd_option.dict_avg_nmec,
            pd_option.dict_avg_nmtc,
            pd_option.dict_avg_fit,
            pd_option.dict_var_fit,
            pd_option.dict_avg_dist,
            pd_option.dict_var_dist,
            pd_option.dict_dist_min,
            pd_option.dict_iqr25_dist,
            pd_option.dict_median_dist,
            pd_option.dict_iqr75_dist,
            pd_option.dict_dist_max,
            pd_option.dict_fit_min,
            pd_option.dict_iqr25_fit,
            pd_option.dict_median_fit,
            pd_option.dict_iqr75_fit,
            pd_option.dict_fit_max]


def get_data_frame():
    global sum_df
    if sum_df is None:
        pd_option.setup_dataframe()
        sum_df = pd.DataFrame(
            columns=get_columns())

    return sum_df


def update_data_frame(values: list):
    df = get_data_frame()
    if len(values) != len(get_columns()) or df is None:
        raise AssertionError(f"DataFrame doesn't exist or number of values ({len(values)} " +
                             f"don't match columns ({len(get_columns())})")

    global sum_df
    sum_df = df.append(dict(zip(get_columns(), values)), ignore_index=True)
