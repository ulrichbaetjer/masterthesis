import pandas as pd


# summary dictionary keys
dict_model = "Model"
dict_orientation = "Orientation"
dict_style = "Style"
dict_scan = "Measure During Drive"
dict_nsr = "Normalized Sample Rate"
dict_size = "Swarm Size"
dict_optim_func = "Optimization Function"
dict_avg_nmec = "Average NMEC"
dict_avg_nmtc = "Average NMTC"
dict_avg_dist = "Average Travelled Distance"
dict_var_dist = "Variance Travelled Distance"
dict_avg_fit = "Average Fitness"
dict_var_fit = "Variance Fitness"
dict_median_dist = "Median Travelled Distance"
dict_iqr25_dist = "Distance IQR_25"
dict_iqr75_dist = "Distance IQR_75"
dict_median_fit = "Median Fitness"
dict_iqr25_fit = "Fitness IQR_25"
dict_iqr75_fit = "Fitness IQR_75"
dict_fit_min = "Fitness Minimum"
dict_fit_max = "Fitness Maximum"
dict_dist_min = "Distance Minimum"
dict_dist_max = "Distance Maximum"

# experiment dictionary keys
dict_fit = "Fitness"
dict_generation = "Generation"
dict_run_number = "Run"
dict_run_seed = "Seed"
dict_avg_path_len = "Average PL"
dict_longest_path_len = "Max PL"


def setup_dataframe():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', None)
    pd.set_option("display.colheader_justify", "left")


def filter_dataframe(df, key, value):

    if key == dict_model:
        filtered_df = df[(df[key] == value)][df.columns] #  | (df[key] == "straight")
        return filtered_df
    else:
        filt = (df[key] == value)
        df_filtered = df[filt]
        return df_filtered


def save_dataframe(df: pd.DataFrame, path):
    df.to_csv(path_or_buf=path, index=False)
