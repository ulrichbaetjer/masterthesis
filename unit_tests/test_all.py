import unittest

from masterthesis.unit_tests.swarm_tests import SwarmTestCases
from masterthesis.unit_tests.optimization_test import OptimizationTestCases
from masterthesis.unit_tests.list_func_tests import MathTestCases
from masterthesis.unit_tests.vehicle_model_test import VehicleModelTestCases


if __name__ == '__main__':
    # initialize test suite
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    # add tests to the test suite
    suite.addTests(loader.loadTestsFromTestCase(SwarmTestCases))
    suite.addTests(loader.loadTestsFromTestCase(OptimizationTestCases))
    suite.addTests(loader.loadTestsFromTestCase(MathTestCases))
    suite.addTests(loader.loadTestsFromTestCase(VehicleModelTestCases))

    # run all loaded tests
    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
