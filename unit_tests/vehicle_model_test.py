import dubins
import reeds_shepp

import unittest
import math
import masterthesis.helper_functions.list_functions as list_func
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.rtr_particle import RTRParticle

class VehicleModelTestCases(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.point_a = [0, 0, 0]
        cls.point_b = [1, 0, 0]
        cls.radius = 1.0
        cls.step_size = 0.2

    def test_dubins_forwards(self):
        dub = dubins.shortest_path(self.point_a, self.point_b, self.radius)
        length = dub.path_length()
        self.assertEqual(length, 1.0)

    def test_reeds_shepp_forwards(self):
        reed = reeds_shepp.PyReedsSheppPath(self.point_a, self.point_b, self.radius)
        length = reed.distance()
        self.assertEqual(length, 1.0)

    def test_dubins_backwards(self):
        dub = dubins.shortest_path(self.point_b, self.point_a, self.radius)
        length = dub.path_length()
        self.assertGreater(length, math.pi * self.radius)

    def test_reeds_shepp_backwards(self):
        reed = reeds_shepp.PyReedsSheppPath(self.point_b, self.point_a, self.radius)
        length = reed.distance()
        self.assertEqual(length, 1.0)

    def test_straight_model(self):
        point_a = [0, 0, 0]
        point_b = [1, 0, 0]
        radius = 1.0

        angle_to_rad = math.pi / 180

        degree_in_circles = 360
        precision = 16
        max = degree_in_circles * precision
        for angle in range(max):
            angle /= precision
            point_b[0] = point_a[0] + math.cos(angle * angle_to_rad) * radius
            point_b[1] = point_a[1] + math.sin(angle * angle_to_rad) * radius

            rad = list_func.calculate_angle(point_a, point_b)
            point_a[2] = rad
            point_b[2] = rad

            reed = reeds_shepp.PyReedsSheppPath(point_a, point_b, radius)
            self.assertAlmostEqual(radius, reed.distance(), delta=1e-15, msg=f"wasn't equal at {angle}")

    def test_straight_no_movement(self):
        point_a = [0.1, 0.1, 0]
        max = round(math.tau * 1000)
        for x in range(max):
            point_a[2] = x / 1000
            reed = reeds_shepp.PyReedsSheppPath(point_a, point_a, 1.0)
            dist = reed.distance()
            self.assertEqual(dist, 0.0, f"wasn't equal with {point_a[2]}")

    def test_rtr(self):
        r = RTRParticle(pos=[1, 0, math.pi])
        r.previous_position = [0, 0, math.pi]
        path = r.create_path(r.previous_position, r.position, r.turn_radius)
        path_length = r.get_path_length(path)
        self.assertEqual(path_length, math.tau, " from (0, 0) to (3, 4) has an euclidean distance of 5.0 ")
        self.assertGreaterEqual(len(path.path), path_length / r.step_size, " path must include points for rotational speed")

    def test_rtr_greedy(self):
        r = RTRParticle(pos=[0, -1, 0])
        next_pos = [0, 1, 0]
        r.orientation_style = OrientationEnums.greedy
        r.update_orientation(next_pos)
        p = r.create_path(r.position, next_pos, 0.2)
        self.assertEqual(r.get_path_length(p), math.pi/2)
