import unittest
import math
import dubins
import masterthesis.helper_functions.list_functions as list_func


class MathTestCases(unittest.TestCase):

    def test_euclidean_distance(self):
        point_a = [0, 0]
        point_b = [0, 0]
        point_c = [1, 0]
        point_d = [1, 1]

        dist_ab = list_func.euclidean_distance(point_a, point_b)
        dist_ac = list_func.euclidean_distance(point_a, point_c)
        dist_ad = list_func.euclidean_distance(point_a, point_d)
        self.assertEqual(dist_ab, 0.0)
        self.assertEqual(dist_ac, 1.0)
        self.assertEqual(dist_ad, math.sqrt(2.0))

    def test_variance_average(self):
        l1 = [1, 2, 3, 4, 5, 6, 7]
        # 28 / 7 = 4
        # (1 - 4)² + (2-4)² + (3-4)² + (4-4)² + (5-4)² + (6-4)² + (7-4)² =
        # 9 + 4 + 1 + 0 + 1 + 4 + 9 = 28
        # 28 / 7 = 4
        var, avg = list_func.calculate_variance_and_average(l1)
        self.assertEqual([var, avg], [4.0, 4.0], "Failed to calculate variance and average")

        l2 = [1, 1, 1, 1, 1, 1, 1]
        var, avg = list_func.calculate_variance_and_average(l2)
        self.assertEqual([var, avg], [0.0, 1.0], "Failed to calculate variance and average")

    def test_round_list(self):
        n = [0.123456, 0.123, 0.123789, 0.1]
        rounded = list_func.round_list(n, 3)
        self.assertEqual(rounded, [0.123, 0.123, 0.124, 0.1], "Failed to round correctly")

    def test_angles(self):
        radius = 1.0
        from_000_to_090, failed1 = self.__quadrant_angle_calc(0, 90, radius)
        from_090_to_180, failed2 = self.__quadrant_angle_calc(90, 180, radius)
        from_180_to_270, failed3 = self.__quadrant_angle_calc(180, 270, radius)
        from_270_to_360, failed4 = self.__quadrant_angle_calc(270, 360, radius)

        self.assertTrue(from_000_to_090, f"Failed these angles {failed1} ... at radius {radius}")
        self.assertTrue(from_090_to_180, f"Failed these angles {failed2} ... at radius {radius}")
        self.assertTrue(from_180_to_270, f"Failed these angles {failed3} ... at radius {radius}")
        self.assertTrue(from_270_to_360, f"Failed these angles {failed4} ... at radius {radius}")

        radius = 0.1
        from_000_to_090, failed1 = self.__quadrant_angle_calc(0, 90, radius)
        from_090_to_180, failed2 = self.__quadrant_angle_calc(90, 180, radius)
        from_180_to_270, failed3 = self.__quadrant_angle_calc(180, 270, radius)
        from_270_to_360, failed4 = self.__quadrant_angle_calc(270, 360, radius)

        self.assertTrue(from_000_to_090, f"Failed these angles {failed1} ... at radius {radius}")
        self.assertTrue(from_090_to_180, f"Failed these angles {failed2} ... at radius {radius}")
        self.assertTrue(from_180_to_270, f"Failed these angles {failed3} ... at radius {radius}")
        self.assertTrue(from_270_to_360, f"Failed these angles {failed4} ... at radius {radius}")

    @staticmethod
    def __quadrant_angle_calc(start_angle, end_angle, radius):
        origin = [0, 0, 0]
        tmp = math.pi / 180

        precision = 14

        angle_range = abs(start_angle - end_angle)
        failed_angles = []
        has_failed = False
        for angle in range(angle_range):
            angle += start_angle
            new_x = origin[0] + math.cos(angle * tmp) * radius
            new_y = origin[1] + math.sin(angle * tmp) * radius
            new_position = [new_x, new_y, 0]

            rad = round(list_func.calculate_angle(origin[:-1], new_position[:-1]), precision)

            origin[-1] = rad
            new_position[-1] = rad
            path = dubins.shortest_path(origin, new_position, radius)
            path_len = round(path.path_length(), precision)
            dist = round(list_func.euclidean_distance(origin[:-1], new_position[:-1]), precision)
            result = dist == path_len
            if not result:
                print(dist, path_len)
                has_failed = True
                failed_angles.append(angle)

        return not has_failed, failed_angles
