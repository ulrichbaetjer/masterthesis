import unittest

from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from math import tau, pi


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.dubins = DubinsParticle(pos=[0, 0, 0])
        self.next_pos = [1, 0, -1]
        self.old_angle = self.next_pos[-1]

    def test_greedy(self):
        self.dubins.orientation_style = OrientationEnums.greedy

        self.dubins.update_orientation(self.next_pos)
        p = self.dubins.create_path(self.dubins.position, self.next_pos, 1.0)

        self.assertNotEqual(self.old_angle, self.next_pos[-1])
        self.assertEqual(1.0, self.dubins.get_path_length(p))

    def test_noupdate(self):
        self.dubins.position[-1] = -1
        self.dubins.orientation_style = OrientationEnums.no_update

        self.dubins.update_orientation(self.next_pos)
        self.assertEqual(self.old_angle, self.next_pos[-1])

    def test_random(self):
        self.dubins.orientation_style = OrientationEnums.random
        self.dubins.update_orientation(self.next_pos)
        self.assertGreaterEqual(self.next_pos[-1], 0)
        self.assertLessEqual(self.next_pos[-1], tau)
        self.assertLess(self.old_angle, 0)

    def test_straight_model_policy(self):
        self.next_pos[0] = -1
        self.dubins.orientation_style = OrientationEnums.straight_model_policy
        self.dubins.update_orientation(self.next_pos)
        self.assertEqual(self.next_pos[-1], pi)


if __name__ == '__main__':
    unittest.main()
