import masterthesis.optimization_function.problem_collection as problems
import unittest
import os


class OptimizationTestCases(unittest.TestCase):

    def test_correct_optima(self):
        problem_dict = problems.get_problems()
        for problemKey in problem_dict:
            prob = problem_dict[problemKey]
            result = prob.evaluate(prob.get_optimum_position())
            self.assertEqual(result, prob.optimal_fitness_value())

    def test_has_all_problems(self):
        problem_dict = problems.get_problems()
        number_of_problems_found = len(os.listdir("../optimization_function/")) - 4     # ignore deap, probcollect,
                                                                                        #  baseproblem and pycache
        existingProblems = len(problem_dict)
        self.assertEqual(existingProblems, number_of_problems_found)


if __name__ == "__main__":
    unittest.main()
