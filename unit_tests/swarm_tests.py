import unittest
from masterthesis.pso.particle import Particle
from masterthesis.pso.vehicles.straight_particle import StraightParticle
from masterthesis.pso.vehicles.dubins_particle import DubinsParticle
from masterthesis.pso.vehicles.reed_shepp_particle import ReedsSheppParticle
from masterthesis.pso.swarm import Swarm
import masterthesis.optimization_function.problem_collection as problems
import math
from random import Random


class SwarmTestCases(unittest.TestCase):

    def test_simple_swarm(self):
        frac = math.tau / 10
        optimize = problems.get_problems()["Ackley"]

        positions = [[-1.5, -1.5, 0], [1, -0.5, frac], [0, 1, frac * 2], [-1.5, 0, frac * 3],
                     [-1.5, 1.5, frac * 4], [0, -1.5, frac * 5], [0, 1.5, frac * 6], [1.5, -1.5, frac * 7],
                     [1.5, 0, frac * 8], [1.5, 1.5, frac * 9]]

        particles = []

        rng = Random(19941208)

        for idx in range(len(positions)):
            particles.append(Particle(positions[idx], inertia=0.5, rand=rng))

        my_swarm = Swarm(particles=particles, optimization_function=optimize, randomness=rng)

        for iteration in range(100):
            my_swarm.update_swarm()

        reached_optimum = my_swarm.get_average_fitness() < 0.00001
        self.assertTrue(reached_optimum, f"fitness was {my_swarm.get_average_fitness()} and not lower than 0.00001")

    def test_same_fitness(self):
        frac = math.tau / 10
        optimize = problems.get_problems()["Ackley"]
        rng1 = Random(19941208)
        rng2 = Random(19941208)
        rng3 = Random(19941208)
        best_pos = optimize.get_optimum_position()
        positions = [[-1.5, -1.5, 0], [1, -0.5, frac], [0, 1, frac * 2], [-1.5, 0, frac * 3],
                     [-1.5, 1.5, frac * 4], [0, -1.5, frac * 5], [0, 1.5, frac * 6], [1.5, -1.5, frac * 7],
                     [1.5, 0, frac * 8], [1.5, 1.5, frac * 9]]

        particles_straight = []
        particles_dubins = []
        particles_reedshepp = []

        for idx in range(len(positions)):
            particles_straight.append(StraightParticle(positions[idx], inertia=0.5, rand=rng1))
            particles_dubins.append(DubinsParticle(positions[idx], dim_size=3, inertia=0.5, rand=rng2))
            particles_reedshepp.append(ReedsSheppParticle(positions[idx], inertia=0.5, rand=rng3))

        straight_swarm = Swarm(particles=particles_straight, optimization_function=optimize, randomness=rng1)
        dubins_swarm = Swarm(particles=particles_dubins, optimization_function=optimize, randomness=rng2)
        reed_swarm = Swarm(particles=particles_reedshepp, optimization_function=optimize, randomness=rng3)

        for iteration in range(10):
            straight_swarm.update_swarm()
            dubins_swarm.update_swarm()
            reed_swarm.update_swarm()

        fit1 = straight_swarm.get_average_fitness()
        fit2 = dubins_swarm.get_average_fitness()
        fit3 = reed_swarm.get_average_fitness()

        self.assertEqual([fit1, fit2], [fit2, fit3], "wasn't equal :(")


if __name__ == '__main__':
    unittest.main()
