# pandas files
import masterthesis.dataframes.pandas_settings as pd_option
import masterthesis.dataframes.summary_dataframe as pd_summary
import masterthesis.dataframes.experiment_dataframe as pd_experiment

# For testing purposes
from masterthesis.optimization_function.ackley_func import AckleyOptimization

# PSO Descriptions
from masterthesis.pso.pso_enums import PsoEnums
from masterthesis.pso.orientation_enums import OrientationEnums
from masterthesis.pso.vehicles.vehicle_struct import VehicleDescription
from masterthesis.pso.swarm import Swarm

# helping functions
import masterthesis.helper_functions.list_functions as list_func
import random
import math
import sys
import os


# evaluation criteria
fitness_threshold = 0.1
max_pso_iterations = 500
start_energy = 100
iteration_limit = 300

# experiment setup
random_seed_swarm = 19941208
number_of_runs = 31
measure_while_driving = False
normalized_sample_rate = False

swarm_size = 10

# tuple keys
distance_key = 0
fitness_key = 1

# active_experiments = (1, 1, 0, 0)


def run(sim, optimization, pso_style, vehicle_type):
    data_frame = pd_summary.get_data_frame()
    style = style_dictionary[sim]
    key_to_sort = get_key(style)

    is_special_model = pso_style == PsoEnums.straight
    experiment = pso_style.name + " with: " if not is_special_model else pso_style.name + " "

    if is_special_model:
        orientation = OrientationEnums.straight_model_policy
        result = start_simulation(sim, optimization, pso_style, orientation, vehicle_type)
        analyze_result(result, key_to_sort, data_frame, pso_style, orientation, optimization, style)
        print(sim, experiment, " done")
    else:
        for orientation in OrientationEnums:
            experiment_n = experiment + str(orientation.name)
            result = start_simulation(sim, optimization, pso_style, orientation, vehicle_type)
            analyze_result(result, key_to_sort, data_frame, pso_style, orientation, optimization, style)
            print(sim, experiment_n, " done")


def start_simulation(sim_func=None, optim_func=None, swarm_type=None, orient_style=None, vehicle_type=None):
    result = []
    seeds = list_func.create_random_seeds(random_seed_swarm, number_of_runs*2)

    for iteration in range(number_of_runs):

        # show progress
        progress = "\r[" + (iteration * "#") + (" " * (number_of_runs-iteration-1)) + "]"
        print(progress, end="")
        sys.stdout.flush()

        # set up correct RNG
        current_swarm_seed = seeds[iteration]
        current_orient_seed = seeds[iteration + 31]
        rand = random.Random(current_swarm_seed)
        rand_orient = random.Random(current_orient_seed)

        # do the run
        avg_trav, avg_fit, nmec, nmtc = run_sim(sim_func, optim_func, swarm_type=swarm_type, orient_style=orient_style,
                                                rand=rand, rand_orient=rand_orient, run_id=(iteration+1),
                                                run_seed=current_swarm_seed, vehicle_type=vehicle_type)
        result.append((avg_trav, avg_fit, nmec, nmtc))

    print("", end="\r")

    if sim_func == simulate_with_max_iterations:

        path = "./results/time_distance"
        if not os.path.isdir(path):
            os.mkdir(path)
        path += f"/{random_seed_swarm}"
        if not os.path.isdir(path):
            os.mkdir(path)
        path += f"/{swarm_type.name}"
        if not os.path.isdir(path):
            os.mkdir(path)
        path += f"/{optim_func.get_name()}"
        if not os.path.isdir(path):
            os.mkdir(path)
        df = pd_experiment.get_data_frame()

        scan_status = "with_scan" if measure_while_driving else "no_scan"
        path += f"/{orient_style.name}_{scan_status}.csv"
        pd_option.save_dataframe(df, path)
        pd_experiment.reset_data_frame()

    return result


# initialize swarm, start the run and return the results
def run_sim(sim_func=None, optim_func=None, swarm_type=None, orient_style=None, rand=None, rand_orient=None, run_id=None, run_seed=None, vehicle_type=None):

    swarm_fuel = start_energy if sim_func == simulate_with_energy else math.inf
    if vehicle_type is None:
        vehicle_type = VehicleDescription()

    vehicle_type.orientation_style = orient_style
    vehicle_type.measure_while_driving = measure_while_driving
    vehicle_type.remaining_energy = swarm_fuel
    vehicle_type.normalize_sampling = normalized_sample_rate

    my_swarm = Swarm(size=swarm_size, optimization_function=optim_func,
                     swarm_type=swarm_type, vehicle_description=vehicle_type,
                     randomness=rand, rand_orientation=rand_orient)

    sim_func(my_swarm, run_id, run_seed)

    average_distance = my_swarm.get_average_distance_travelled()

    best_fitness = optim_func.evaluate(my_swarm.best_found_position)
    return average_distance, best_fitness, my_swarm.calculate_nmec(), my_swarm.calculate_nmtc()


def simulate_with_fitness_threshold(my_swarm=None, run_id=None, seed=None):

    while my_swarm.current_generation < max_pso_iterations:

        my_swarm.update_swarm()

        if my_swarm.best_fitness_value <= fitness_threshold:
            break


def simulate_with_max_iterations(my_swarm=None, run_id=None, seed=None):
    pd_experiment.update_data_frame(pd_experiment.extract_swarm_data(my_swarm, run_id, seed))

    while my_swarm.current_generation < iteration_limit:
        my_swarm.update_swarm()
        values = pd_experiment.extract_swarm_data(my_swarm, run_id, seed)
        pd_experiment.update_data_frame(values)


def simulate_with_energy(my_swarm=None, run_id=None, seed=None):
    while my_swarm.has_swarm_moved() and my_swarm.current_generation < max_pso_iterations:
        my_swarm.update_swarm()


# write the pandas dataframe for the summary of the 31 runs
def analyze_result(result=None, key_to_sort=distance_key, data_frame=None, model=None, orient=None, optim_func=None, style=None):
    if isinstance(result, list):
        # result = list(filter(lambda x: x[0] is not math.nan, result))

        len_res = len(result)

        average_travelled, variance_travelled = math.nan, math.nan
        average_fitness, variance_fitness = math.nan, math.nan

        avg_nmec, avg_nmtc = math.nan, math.nan

        median_index = [math.nan, math.nan, math.nan, math.nan]
        iqr_25 = [math.nan, math.nan, math.nan, math.nan]
        iqr_75 = [math.nan, math.nan, math.nan, math.nan]

        fit_min = math.nan
        fit_max = math.nan
        dist_min = math.nan
        dist_max = math.nan

        if len_res > 0:
            travel, fitness, nmecs, nmtcs = list(zip(*result))
            variance_travelled, average_travelled = list_func.calculate_variance_and_average(travel)
            variance_fitness, average_fitness = list_func.calculate_variance_and_average(fitness)
            avg_nmec, _ = list_func.calculate_variance_and_average(nmecs)
            avg_nmtc, _ = list_func.calculate_variance_and_average(nmtcs)

            iqr25_index = len_res // 4

            access_key = distance_key
            result.sort(key=lambda tup: tup[access_key])
            # all_fitness = [r[fitness_key] for r in result]
            # print(all_fitness)

            dist_min = result[0][access_key]
            iqr_25[access_key] = result[iqr25_index][access_key]
            median_index[access_key] = result[len_res // 2][access_key]
            iqr_75[access_key] = result[(len_res-1) - iqr25_index][access_key]
            dist_max = result[-1][access_key]

            # key_to_sort = fitness_key
            access_key = fitness_key
            result.sort(key=lambda tup: tup[access_key])

            fit_min = result[0][access_key]
            median_index[access_key] = result[len_res // 2][access_key]
            iqr_25[access_key] = result[iqr25_index][access_key]
            iqr_75[access_key] = result[(len_res - 1) - iqr25_index][access_key]
            fit_max = result[-1][access_key]

        mwd = "True" if measure_while_driving else "False"

        # no point having nsr True if mwd is false
        if not measure_while_driving:
            nsr = False
        nsr = "True" if normalized_sample_rate else "False"

        result_list = [model.name,
                       orient.name,
                       style,
                       mwd,
                       nsr,
                       swarm_size,
                       optim_func.get_name(),
                       avg_nmec,
                       avg_nmtc,
                       average_fitness,
                       variance_fitness,
                       average_travelled,
                       variance_travelled,
                       dist_min,
                       iqr_25[distance_key],
                       median_index[distance_key],
                       iqr_75[distance_key],
                       dist_max,
                       fit_min,
                       iqr_25[fitness_key],
                       median_index[fitness_key],
                       iqr_75[fitness_key],
                       fit_max]
        pd_summary.update_data_frame(result_list)

    else:
        raise TypeError("Expected list, got: ", type(result))


def get_key(style):
    if style == "energy":
        return fitness_key
    else:
        return distance_key


style_dictionary = {simulate_with_fitness_threshold: "fitness",
                    simulate_with_energy: "energy",
                    simulate_with_max_iterations: "iterations"}
